<?php

/**
 * If debugging is on then 
 * turn on PHP errors.
 */
if(true===AGSPP_DEBUG) :
    /**
     * Turn on PHP errors.
     */
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
endif;

/**
 * Instantiate Agspp DI Container Class.
 */
include AGSPP_INCLUDES_PATH.'DI_Container.php';
$DI_Container = new \Agspp\Di_Container();

/**
 * Load Includes.
 */
$DI_Container->loadInclude('service_base', 'ServiceBase');

/**
 * Load helpers.
 */
$DI_Container->loadHelper('agspp-helpers', 'helpers');
$DI_Container->loadHelper('agspp-forms', 'forms');
$DI_Container->loadHelper('agspp-shortcodes', 'shortcodes');

/**
 * Load interfaces.
 */
$DI_Container->loadInterface('service_interface', 'ServiceInterface');

/**
 * Load services.
 */
$DI_Container->loadService('agspp', 'Agspp');
$DI_Container->loadService('agspp_admin', 'Agspp_Admin');
$DI_Container->loadService('ajax', 'Ajax');
$DI_Container->loadService('form', 'Form');
$DI_Container->loadService('html', 'Html');
$DI_Container->loadService('portfolio', 'Portfolio');
$DI_Container->loadService('sendinblue', 'Sendinblue');
$DI_Container->loadService('sessions', 'Sessions');
$DI_Container->loadService('spot_price', 'SpotPrice');
$DI_Container->loadService('user', 'User');
$DI_Container->loadService('form-validator', 'Form_Field_Validator');
$DI_Container->loadService('wishlist', 'Wishlist');
$DI_Container->loadService('woocommerce', 'WooCommerce');

/**
 * Register Services
 */

/** Register plugin controller */
$DI_Container->registerService('agspp', 'Agspp\Services\Agspp', [
    'spot_price',
    'wishlist',
    'portfolio',
    'form'
]);

/** Register ajax service. */
$DI_Container->registerService('ajax', 'Agspp\Services\Ajax', [
    'spot_price',
    'portfolio'
]);

/** Register form service. */
$DI_Container->registerService('form', 'Agspp\Services\Form');

/** Register html service. */
$DI_Container->registerService('html', 'Agspp\Services\Html', []);

/** Register portfolio service. */
$DI_Container->registerService('portfolio', 'Agspp\Services\Portfolio', []);

/** Register spot price service. */
$DI_Container->registerService('spot_price', 'Agspp\Services\SpotPrice', []);

/** Register user service. */
$DI_Container->registerService('user', 'Agspp\Services\User', []);

/** Register wishlist service. */
$DI_Container->registerService('wishlist', 'Agspp\Services\Wishlist', []);

/** Register woocommerce service. */
$DI_Container->registerService('woocommerce', 'Agspp\Services\WooCommerce', [
    'portfolio',
    'wishlist'
]);