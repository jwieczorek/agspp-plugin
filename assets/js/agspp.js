jQuery(function( $ ) {  
    
    if($('div.agspp-spot-prices')!=='undefined') 
    {
        $url = $('div.agspp-spot-prices').data('url');
        
        $.ajax({
            url : $url,
            type    : 'POST',
            data    : {
                action  : "ajax_spot_prices"            
            },
            success : function(response) {
                data = JSON.parse(response);
                $('span.spot-price-gold').text(data.gold);
                $('span.spot-price-silver').text(data.silver);
                $('span.spot-price-platinum').text(data.platinum);
                $('span.spot-price-palladium').text(data.palladium);                
            }
        });
    }
    
    
    
    if($('button.ajax-email-button')!=='undefined')
    {
        $('button.ajax-email-button').click(function(){       
        
            $email = $('input#ajax-email-signup-input').val();
            
            if(''===$email) {
                alert('Please enter your email address.');
                return;
            }
        
            $.ajax({
                url : $(this).data('url'),
                type    : 'POST',
                data    : {
                    action  : "ajax_email_signup",
                    email   : $email
                },
                success : function(response) {
                    data = JSON.parse(response);
                    alert(data.message);
                    
                    if('success'===data.status) {
                        $('input#ajax-email-signup-input').val('');
                    }
                }
            });
        });   
    }
    

    

    function agspp_portfolio_chart(data)
    {
        Highcharts.setOptions({
            colors: ['#CC7C04', '#CFCFCC', '#DBDCDD', '#B5B7B4']
        });

        Highcharts.chart('user-metals-portfolio-chart-container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Your Metals Portfolio'
            },
            tooltip: {
                pointFormat: '{point.percentage:.1f}%'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [
                {
                    name: 'Metals',
                    colorByPoint: true,
                    data: [
                        {
                            name: 'Gold',
                            y: parseFloat(data.gold)
                        },
                        {
                            name: 'Silver',
                            y: parseFloat(data.silver),
                        },
                        {
                            name: 'Platinum',
                            y: parseFloat(data.platinum)
                        },
                        {
                            name: 'Palladium',
                            y: parseFloat(data.palladium)
                        }
                    ]
                }
            ]
        });
    }


    $agspp_url = null;
    $agspp_uid = null;

    if( $('#user-metals-portfolio-chart-container') !== null )
    {
        $agspp_url = $('#user-metals-portfolio-chart-container').data('url');
        $agspp_uid = $('#user-metals-portfolio-chart-container').data('uid');
    }

    if( $agspp_url && $agspp_uid )
    {
        $.ajax({
            url : $agspp_url,
            type    : 'POST',
            data    : {
                action  : "portfolio_request",
                uid     : $agspp_uid
            },
            success : function(response) {

                if( response === 'error' ) {
                    alert('An error occured please contact info@agspp.com!');
                    return;
                }

                data = JSON.parse(response);

                agspp_portfolio_chart(data);
            }
        });
    }
});


jQuery(function( $ ) {  
    $(document.body).on('change', 'input[name="payment_method"]', function() {
        $('body').trigger('update_checkout');
    });
});

 