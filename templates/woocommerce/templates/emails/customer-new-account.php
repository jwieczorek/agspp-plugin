<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$user_email = $user_login;
$user       = get_user_by('login', $user_login);

if ( $user ) {
    $user_email = $user->user_email;
}

?>

<?php do_action( 'woocommerce_email_header', '' , $email ); ?>

    <p>Welcome to your new  American Gold | SPP account. </p>

    <p>This is just a quick note to thank you for taking the time to invest in your future with our company. We take your time and future very seriously. Our assignment is to help you accumulate wealth through precious metals and again we are happy you have chosen us.    </p>

    <p>As you familiarize yourself with our precious metal collections, we hope, in the years to come we have helped you build an investment that will very well serve your needs and those of your family.</p>

    <p>Once again, our congratulations on becoming part of our growing national family and making the best financial decision of your life. We look forward to seeing your wealth grow and stand the tests of time.</p>

    <p>As you should know, your login credentials are private so please exercise caution with your account. You can them find them below:</p>

    <p><strong>Login email</strong>: <em><?php echo $user_email; ?></em></p>

    <p><strong>Login password</strong>: <em><?php echo isset($password) ? $password : 'the password you chose'; ?></em></p>

    <p>You may login here <a href="https://agspp.com/login">agspp.com</a></p>

    <p>Sincerely,</p>

    <p>American Gold Platinum and Palladium</p>

<?php do_action( 'woocommerce_email_footer', $email );
