<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $AGSPP_USER;

?>

<h4>Hi <?php echo $AGSPP_USER->meta('first_name') ?>, what would you like to do?</h4>

<div class="row"> 
    
    <div class="col-md-4">
        <a href="/my-account/orders" target="_self" class="kad-icon-box-39 kad-icon-box ">
            <i class="icon-bag" style="font-size:48px;"></i>
            <h4>My Orders</h4>
            <p><em>Checkout all of your orders here!</em></p>
        </a>
        <style type="text/css" media="screen">.kad-icon-box-39 {background:#dddddd;} .kad-icon-box-39, .kad-icon-box-39 h1, .kad-icon-box-39 h2, .kad-icon-box-39 h3, .kad-icon-box-39 h4, .kad-icon-box-39 h5 {color:#000000 !important;} .kad-icon-box-39:hover {} .kad-icon-box-39:hover, .kad-icon-box-39:hover h1, .kad-icon-box-39:hover h2, .kad-icon-box-39:hover h3, .kad-icon-box-39:hover h4, .kad-icon-box-39:hover h5 {color:#ffffff !important;}</style>
    </div>
    
    <div class="col-md-4  ">
        <a href="/my-account/metals-portfolio" target="_self" class="kad-icon-box-44 kad-icon-box ">
            <i class="icon-folder" style="font-size:48px;"></i>
            <h4>My Portfolio</h4>
            <p><em>Count your worth!</em></p>
        </a>
        <style type="text/css" media="screen">.kad-icon-box-44 {background:#dddddd;} .kad-icon-box-44, .kad-icon-box-44 h1, .kad-icon-box-44 h2, .kad-icon-box-44 h3, .kad-icon-box-44 h4, .kad-icon-box-44 h5 {color:#000000 !important;} .kad-icon-box-44:hover {} .kad-icon-box-44:hover, .kad-icon-box-44:hover h1, .kad-icon-box-44:hover h2, .kad-icon-box-44:hover h3, .kad-icon-box-44:hover h4, .kad-icon-box-44:hover h5 {color:#ffffff !important;}</style>
    </div>
    
    <div class="col-md-4  ">
        <a href="/my-account/wishlist" target="_self" class="kad-icon-box-57 kad-icon-box ">
            <i class="icon-stack-list" style="font-size:48px;"></i>
            <h4>My Wishlist</h4>
            <p><em>View your wishlist!</em></p>
        </a>
        <style type="text/css" media="screen">.kad-icon-box-57 {background:#dddddd;} .kad-icon-box-57, .kad-icon-box-57 h1, .kad-icon-box-57 h2, .kad-icon-box-57 h3, .kad-icon-box-57 h4, .kad-icon-box-57 h5 {color:#000000 !important;} .kad-icon-box-57:hover {} .kad-icon-box-57:hover, .kad-icon-box-57:hover h1, .kad-icon-box-57:hover h2, .kad-icon-box-57:hover h3, .kad-icon-box-57:hover h4, .kad-icon-box-57:hover h5 {color:#ffffff !important;}</style>
    </div>
    
</div>


<div class="row"> 
    
    <div class="col-md-6">
        <a href="/my-account/edit-address/" target="_self" class="kad-icon-box-39 kad-icon-box ">
            <i class="icon-address-book" style="font-size:48px;"></i>
            <h4>My Addresses</h4>
            <p><em>View and edit your addresses!</em></p>
        </a>
        <style type="text/css" media="screen">.kad-icon-box-39 {background:#dddddd;} .kad-icon-box-39, .kad-icon-box-39 h1, .kad-icon-box-39 h2, .kad-icon-box-39 h3, .kad-icon-box-39 h4, .kad-icon-box-39 h5 {color:#000000 !important;} .kad-icon-box-39:hover {} .kad-icon-box-39:hover, .kad-icon-box-39:hover h1, .kad-icon-box-39:hover h2, .kad-icon-box-39:hover h3, .kad-icon-box-39:hover h4, .kad-icon-box-39:hover h5 {color:#ffffff !important;}</style>
    </div>
    
    <div class="col-md-6">
        <a href="/my-account/contact-us" target="_self" class="kad-icon-box-44 kad-icon-box ">
            <i class="icon-phone" style="font-size:48px;"></i>
            <h4>Contact us</h4>
            <p><em>Have a question or need help</em></p>
        </a>
        <style type="text/css" media="screen">.kad-icon-box-44 {background:#dddddd;} .kad-icon-box-44, .kad-icon-box-44 h1, .kad-icon-box-44 h2, .kad-icon-box-44 h3, .kad-icon-box-44 h4, .kad-icon-box-44 h5 {color:#000000 !important;} .kad-icon-box-44:hover {} .kad-icon-box-44:hover, .kad-icon-box-44:hover h1, .kad-icon-box-44:hover h2, .kad-icon-box-44:hover h3, .kad-icon-box-44:hover h4, .kad-icon-box-44:hover h5 {color:#ffffff !important;}</style>
    </div>
    
</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );
