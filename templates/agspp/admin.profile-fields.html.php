<h3>Security & Activity Information</h3>
<table class="form-table">
    <tr>
        <th>
            <label for="reg-ip">Registration Date</label>
        </th>
	<td>
            <?php echo esc_attr(get_the_author_meta('_register_date', $user->ID)); ?>            
	</td>
    </tr>
    
    <tr>
        <th>
            <label for="reg-ip">Registration IP</label>
        </th>
	<td>
            <?php echo esc_attr(get_the_author_meta('_register_ip', $user->ID)); ?>            
	</td>
    </tr>
    
    <tr>
	<th>
            <label for="last-login">Last Login</label>
	</th>
        <td>
            <?php echo esc_attr(get_the_author_meta('_last_login', $user->ID)); ?>
	</td>
    </tr>
    
    <tr>
	<th>
            <label for="last-login-ip">Last Login IP</label>
	</th>
	<td>
            <?php echo esc_attr(get_the_author_meta('_last_login_ip', $user->ID)); ?>		
	</td>
    </tr>
</table>