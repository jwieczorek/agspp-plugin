<h2 class="kad_endpointtitle">Your Wishlist</h2>
<?php if(!empty($wishlist)): ?>
<table class="woocommerce-MyAccount-wishlist shop_table shop_table_responsive account-whishlist-table">
    <thead>
        <tr>
            <td>Date</td>
            <td>Product</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($wishlist as $item): 
            $delete_link        = add_query_arg(['wishlist'=>'delete','product'=>$item->product_id,'redirect'=>urlencode(site_url(get_option('agspp_page_user_account').'/wishlist'))]);
            $add_to_cart_link   = add_query_arg('add-to-cart',$item->product_id);        
        ?>
        <tr>
            <td>
                <?php echo date("D, M j, Y g:i a",strtotime($item->date_added)); ?>
            </td>
            <td>
                <?php echo get_the_title($item->product_id); ?>
            </td>
            <td class="order-actions ">
                <a href="<?php echo $delete_link; ?>" class="button wishlist-button remove">Remove</a>                
                <a href="<?php echo get_the_permalink($item->product_id); ?>" class="button wishlist-button view">View</a>
                <a href="<?php echo $add_to_cart_link ?>" class="button wishlist-button add-to-cart">Add To Cart</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php else: ?>
<p>You have nothing in your wishlist. <a href="<?php echo site_url('shop') ?>">Click here to begin browsing.</a></p>
<?php endif; ?>
