<div id="wrapper">
    
    <div id="header" style="text-align: center;border-top:1px solid #000;border-bottom:1px solid #000">
        <a href="https://agspp.com">
            <img style="max-width: 250px;" src="http://2017.agspp.com/wp-content/uploads/AGSPP-logo.png" alt="" />
        </a>
    </div>
    
    <div id="content">
        
        <p>Welcome to your new  American Gold | SPP account. </p>
        
        <p>This is just a quick note to thank you for taking the time to invest in your future with our company. We take your time and future very seriously. Our assignment is to help you accumulate wealth through precious metals and again we are happy you have chosen us.    </p>

        <p>As you familiarize yourself with our precious metal collections, we hope, in the years to come we have helped you build an investment that will very well serve your needs and those of your family.</p>

        <p>Once again, our congratulations on becoming part of our growing national family and making the best financial decision of your life. We look forward to seeing your wealth grow and stand the tests of time.</p>
        
        <p>As you should know, your login credentials are private so please exercise caution with your account. You can them find them below:</p>
        
        <p><strong>Login email</strong>: <em>{{email}}</em></p>
        
        <p><strong>Login password</strong>: <em>{{password}}</em></p>
        
        <p>You may login here <a href="https://agspp.com/login">agspp.com</a></p>
        
        <p>Sincerely,</p>
        
        <p>American Gold Platinum and Palladium</p>
        
    </div>
    
    <div id="footer" style="background-color: #000000; text-align: center;">
        <p style="color: #ffffff; padding: 10px;">
            &copy;2017 <a style="color: #ffffff; text-decoration: none;" href="https://agspp.com">American Gold Silver Platinum and Palladium</a>
        </p>
    </div>
    
</div>