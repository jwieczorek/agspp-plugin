<div id="wrapper">
    
    <div id="header" style="text-align: center;border-top:1px solid #000;border-bottom:1px solid #000">
        <a href="https://agspp.com">
            <img style="max-width: 250px;" src="http://2017.agspp.com/wp-content/uploads/AGSPP-logo.png" alt="" />
        </a>
    </div>
    
    <div id="content">
        
        <p>If you did not request to reset your password please disregard this notice.</p>
        
        <p>We are sorry to hear you forgot your password. Hopefully the link below will help you regain access to your account.</p>
        
        <p><a href="{{link}}">Click here to reset your password!</a></p>
        
        <p>If this link does not work, copy and paste the url below into your browser's address bar.</p>
        
        <p>{{link}}</p>        
        
        <p>Sincerely,</p>
        
        <p>Account Security Team</p>
        
        <p>American Gold Platinum and Palladium</p>
        
    </div>
    
    <div id="footer" style="background-color: #000000; text-align: center;">
        <p style="color: #ffffff; padding: 10px;">
            &copy;2017 <a style="color: #ffffff; text-decoration: none;" href="https://agspp.com">American Gold Silver Platinum and Palladium</a>
        </p>
    </div>
    
</div>