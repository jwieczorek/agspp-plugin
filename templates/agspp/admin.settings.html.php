<div class="wrap">
    <h1>American Gold | SPP Settings</h1>
    
    <form method="post">
        
        <table class="form-table">
        
            <tbody>              
                
                <!-- PAYPAL/CC CHARGE -->
                <tr>                    
                    <td>
                        <label for="phone-number">AGSPP Phone Number</label>                        
                    </td>
                    <td>
                        <input class="regular-text" id="phone-number" type="text" name="agspp_options[agspp_phone_number]" value="<?php echo get_option('agspp_phone_number'); ?>" />
                    </td>                    
                </tr>
                <!-- #PAYPAL/CC CHARGE -->


                <!-- PAYPAL/CC CHARGE -->
                <tr>                    
                    <td>
                        <label for="paypal-cc-fee">PayPal/CC Extra Charge</label>                        
                    </td>
                    <td>
                        <input class="regular-text" id="paypal-cc-fee" type="text" name="agspp_options[agspp_cc_paypal_charge]" value="<?php echo get_option('agspp_cc_paypal_charge'); ?>" />
                    </td>                    
                </tr>
                <!-- #PAYPAL/CC CHARGE -->
                
                
                <!-- CUSTOMER CONTACT FORM -->
                <tr>                    
                    <td>
                        <label for="customer-contact-form">Customer Contact Form ID</label>                        
                    </td>
                    <td>
                        <input class="regular-text" id="customer-contact-form" type="text" name="agspp_options[agspp_customer_contact_form]" value="<?php echo get_option('agspp_customer_contact_form'); ?>" />
                        <p class="description">Shortcode for form.</p>
                    </td>                    
                </tr>
                <!-- #CUSTOMER CONTACT FORM -->
                
                <!-- PRODUCT NOTICE -->
                <tr>                    
                    <td>
                        <label for="product-notice">Product Notice</label>                        
                    </td>
                    <td>
                        <input class="regular-text" id="product-notice" type="text" name="agspp_options[agspp_before_product_info_text]" value="<?php echo esc_html(get_option('agspp_before_product_info_text')); ?>" />                        
                        <p class="description">Notice above bulk pricing table.</p>
                    </td>                    
                </tr>
                <!-- #PRODUCT NOTICE -->

                <!-- SUBMIT BUTTON -->
                <tr>
                    <td colspan="2">
                        <button class="button button-primary">Save</button>
                    </td>
                </tr>
                <!-- #SUBMIT BUTTON -->

            </tbody>

        </table>
        <input type="hidden" name="agspp-admin-form" value="settings"/>
    </form>
    
</div>



