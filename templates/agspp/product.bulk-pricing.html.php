<?php if($metal && !empty($tiers)): ?>
<table class="shop_table shop_table_responsive bulk-pricing-table">
    <thead>
        <tr>
            <td>Quantity</td>
            <td>Check/Wire</td>
            <td>CC/PayPal</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($tiers as $tier): 
            if(''!=$tier['from']):
                $total_price = $tier['price'] + $spot_price;
        ?>
        <tr>
            <td>
                <?php echo $tier['from'];?><?php echo ($tier['to'] != '') ? '-'.$tier['to'] : '+'; ?>
            </td>
            <td>
                $<?php echo number_format(round($total_price ,2),2);?>
            </td>
            <td>
                $<?php echo number_format(round(($total_price+($total_price*$cc_pp_cg)) ,2),2);?>
            </td>
        </tr>
        <?php endif; endforeach; ?>
    </tbody>
</table>
<?php endif; 
