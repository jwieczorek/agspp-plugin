<form class="agspp-form" method="POST">
    
    <?php if($AGSPP->form->has_error('invalid-captcha')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('invalid-captcha');?></p>
    <?php endif; ?>
    
    <?php if($AGSPP->form->has_error('message')): ?>    
        <p class="alert alert-info" role="alert"><?php echo $AGSPP->form->error('message');?></p>
    <?php endif; ?>   
        
        
    <p class="form-group">
        <label for="first-name">First Name</label>
        <input id="first-name" class="form-control" type="text" name="first_name" required="required" value="<?php echo $AGSPP->form->field('first_name');?>" />
    </p>  
    <?php if($AGSPP->form->has_error('first_name')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('first_name');?></p>
    <?php endif; ?>
    
        
    <p class="form-group">
        <label for="last-name">Last Name</label>
        <input id="last-name" class="form-control" type="text" name="last_name" required="required" value="<?php echo $AGSPP->form->field('last_name');?>" />
    </p>  
    <?php if($AGSPP->form->has_error('last_name')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('last_name');?></p>
    <?php endif; ?>
        
    <p class="form-group">
        <label for="email">Email</label>
        <input id="email" class="form-control" type="text" name="email" required="required" value="<?php echo $AGSPP->form->field('email');?>" />
    </p>  
    <?php if($AGSPP->form->has_error('email')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('email');?></p>
    <?php endif; ?>
        
    <p class="form-group">
        <label for="email">Re-enter Email</label>
        <input id="email" class="form-control" type="text" name="email_confirm" required="required" value="<?php echo $AGSPP->form->field('email_confirm');?>" />
    </p>  
    
    <div class="g-recaptcha" data-sitekey="<?php echo get_option('agspp_recaptcha_site'); ?>"></div>
    
    <p style="margin-top:10px">
        <button class="btn kad-btn-primary button">Signup</button>
    </p>   
    
    <div style="position:fixed;display:none;visibility:hidden;width:0px;height:0px;left:-1000000000000px;top:-1000000000000px;">
        <?php wp_nonce_field('agspp_nonce_login','agspp_nonce_login'); ?>
        <input type="hidden" name="redirect" value="<?php echo $redirect_url; ?>" />
        <input type="hidden" name="sib_lists_add" value="<?php echo $atts['lists'] ?>" />
        <input type="hidden" name="agspp-form" value="sib-signup" />
        <input type="hidden" name="color" value="" />
    </div>
    
</form>