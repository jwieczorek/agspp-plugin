<form class="agspp-form" method="POST">
    
    <p style="text-align: center">
        Create an AGSPP account to check out faster, track your orders, receive special offers and take advantage of our exclusive tools.
    </p>
    
    <?php if($AGSPP->form->has_error('message')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('message');?></p>
    <?php endif; ?>
    
    <p class="form-group">
        <label for="first-name">First Name</label>
        <input id="first-name" class="form-control" type="text" name="first_name" required="required" value="<?php echo $AGSPP->form->field('first_name');?>" />
    </p>    
    <?php if($AGSPP->form->has_error('first_name')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('first_name');?></p>    
    <?php endif; ?>
        
    
    <p class="form-group">
        <label for="last-name">Last Name</label>
        <input id="last-name" class="form-control" type="text" name="last_name" required="required" value="<?php echo $AGSPP->form->field('last_name');?>" />
    </p>    
    <?php if($AGSPP->form->has_error('last_name')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('last_name');?></p>    
    <?php endif; ?>
        
    
    <p class="form-group">
        <label for="email">Email</label>
        <input id="email" class="form-control" type="text" name="email" required="required" value="<?php echo $AGSPP->form->field('email');?>" />
    </p>    
    <?php if($AGSPP->form->has_error('email')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('email');?></p>    
    <?php endif; ?>
        
        
    <p class="form-group">
        <label for="email-confirm">Re-enter Your Email</label>
        <input id="email-confirm" class="form-control" type="text" name="email_confirm" required="required" value="<?php echo $AGSPP->form->field('email_confirm');?>" />
    </p>    
    <?php if($AGSPP->form->has_error('email_confirm')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('email_confirm');?></p>    
    <?php endif; ?>
        
        
    <p class="form-group">
        <label for="password">Password</label>
        <input id="password" class="form-control" type="password" name="password" required="required" />
    </p>    
    <?php if($AGSPP->form->has_error('password')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('password');?></p>
    <?php endif; ?>
        
        
        
    <p class="form-group">
        <label for="password-confirm">Re-enter Your Password</label>
        <input id="password-confirm" class="form-control" type="password" name="password_confirm" required="required" />
    </p>   
    
    <div class="g-recaptcha" data-sitekey="<?php echo get_option('agspp_recaptcha_site'); ?>"></div>
    
    <p class="checkbox">
        <label for="email_signup">
            <input id="email_signup" class="checkbox" type="checkbox" name="email_signup" checked="checked" />: Yes! Sign me up for special offers and breaking market precious metals news emails.
        </label>
    </p>   
    
    <p>
        <button class="btn kad-btn-primary button">Register</button>
    </p>
    
    <p style="font-weight:800">        
        By creating an account you agree to AGSPP.com's <a href="<?php echo site_url('user-agreement');?>" target="_blank">User Agreement</a> and <a href="<?php echo site_url('privacy-policy');?>" target="_blank">Privacy Policy</a>.
    </p>
    
    <p>
        <a href="<?php echo site_url('login'); ?>" class="link link-register">Click Here to Login!</a>
    </p>   
    
    <div style="position:fixed;display:none;visibility:hidden;width:0px;height:0px;left:-1000000000000px;top:-1000000000000px;">
        <?php wp_nonce_field('agspp_form_nonce', 'agspp_form_nonce'); ?>
        <input type="hidden" name="redirect" value="<?php echo $redirect_url; ?>" />
        <input type="hidden" name="sib_lists_add" value="<?php echo $atts['lists'] ?>" />
        <input type="hidden" name="agspp-form" value="register" />
        <input type="hidden" name="color" value="" />
    </div>
    
</form>
