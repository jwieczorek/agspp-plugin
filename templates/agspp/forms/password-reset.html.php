<form class="agspp-form" method="POST">
    
    <?php if($AGSPP->form->has_error('error')): ?>
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('error');?></p>
    <?php endif; ?>
        
    <p class="form-group">
        <label for="password">New Password</label>
        <input id="password" class="form-control" type="password" name="password" required="required" />
    </p>  
    <?php if($AGSPP->form->has_error('password')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('password');?></p>
    <?php endif; ?>
    
    <p class="form-group">
        <label for="password-confirm">Confirm Password</label>
        <input id="password-confirm" class="form-control" type="password" name="password_confirm" required="required" />
    </p>  
  
    
    <p>
        <button class="btn kad-btn-primary button">Reset Password</button>
    </p>
    
    <div style="position:fixed;display:none;visibility:hidden;width:0px;height:0px;left:-1000000000000px;top:-1000000000000px;">
        <?php wp_nonce_field('agspp_form_nonce','agspp_form_nonce'); ?>
        <input type="hidden" name="redirect" value="<?php echo $redirect_url; ?>" />
        <input type="hidden" name="agspp-form" value="password-reset" />
        <input type="hidden" name="color" value="" />
    </div>
    
</form>