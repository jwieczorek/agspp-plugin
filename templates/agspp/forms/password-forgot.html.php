<form class="agspp-form" method="POST">
    
    <?php if($AGSPP->form->has_error('invalid-captcha')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('invalid-captcha');?></p>
    <?php endif; ?>
    
    <?php if($AGSPP->form->has_error('message')): ?>    
        <p class="alert alert-info" role="alert"><?php echo $AGSPP->form->error('message');?></p>
    <?php endif; ?>
        
    <p class="form-group">
        <label for="email">Email/Username</label>
        <input id="email" class="form-control" type="text" name="email" required="required" />
    </p>
    
    <div class="g-recaptcha" data-sitekey="<?php echo get_option('agspp_recaptcha_site'); ?>"></div>
    
    <p style="margin-top: 10px">
        <button class="btn kad-btn-primary button">Send Password Reset Link</button>
    </p>
    
    <p>
        <a href="<?php echo site_url('login'); ?>" class="link link-login">Click Here to Login!</a>
    </p>  
    
    <div style="position:fixed;display:none;visibility:hidden;width:0px;height:0px;left:-1000000000000px;top:-1000000000000px;">
        <?php wp_nonce_field('agspp_nonce_pass_forgot','agspp_nonce_pass_forgot'); ?>
        <input type="hidden" name="redirect" value="<?php echo $redirect_url; ?>" />
        <input type="hidden" name="agspp-form" value="password-forgot" />
        <input type="hidden" name="color" value="" />
    </div>
    
</form>