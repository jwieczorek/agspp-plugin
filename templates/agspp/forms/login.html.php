<form class="agspp-form" method="POST">   
    
    <?php if($AGSPP->form->has_error('message')): ?>    
        <p class="alert alert-danger" role="alert"><?php echo $AGSPP->form->error('message');?></p>
    <?php endif; ?>
        
        
    <p class="form-group">
        <label for="email">Email/Username</label>
        <input id="email" class="form-control" type="text" name="email" required="required" value="<?php echo $AGSPP->form->field('email');?>" />
    </p>           
        
    <p class="form-group">
        <label for="password">Password</label>
        <input id="password" class="form-control" type="password" name="password" required="required" />
    </p>
  
    <div class="g-recaptcha" data-sitekey="<?php echo get_option('agspp_recaptcha_site'); ?>"></div>
        
    <p class="checkbox">
        <label for="remember">
            <input id="remember" class="checkbox" type="checkbox" name="remember" />: Remember Me
        </label>
    </p> 
    
    <p>
        <button class="btn kad-btn-primary button">Login</button>
    </p>
    
    <p>
        <a href="<?php echo site_url('register'); ?>" class="link link-register">Click Here to Register!</a>
    </p>
    
    <p>        
        <a href="<?php echo site_url('password-forgot'); ?>" class="link link-forgot-pass">Forgot your password?</a>
    </p>    
    
    <div style="position:fixed;display:none;visibility:hidden;width:0px;height:0px;left:-1000000000000px;top:-1000000000000px;">
        <?php wp_nonce_field('agspp_nonce_login','agspp_nonce_login'); ?>
        <input type="hidden" name="redirect" value="<?php echo $redirect_url; ?>" />
        <input type="hidden" name="agspp-form" value="login" />
        <input type="hidden" name="color" value="" />
    </div>
    
</form>