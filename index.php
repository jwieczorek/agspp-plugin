<?php
/*
Plugin Name: AGSPP - American Gold | SPP
Version: 1.0.0
Author: Joshua Wieczorek
Author URI: http://www.joshuawieczorek.com
Description: Custom plugin for AGSPP
*/

/**
 * Turn on/off debugging. [true/false]
 */
defined('AGSPP_DEBUG') || define('AGSPP_DEBUG', true);

/**
 * Define all paths and urls.
 */
defined('AGSPP_PATH') || define('AGSPP_PATH', dirname(__FILE__).'/');
// Plugin url.
defined('AGSPP_PLUGIN_URL') || define('AGSPP_PLUGIN_URL', plugin_dir_url(__FILE__));
// Install path
defined('AGSPP_INSTALL_PATH') || define('AGSPP_INSTALL_PATH', dirname(__FILE__).'/install/');
// Includes path.
defined('AGSPP_INCLUDES_PATH') || define('AGSPP_INCLUDES_PATH', dirname(__FILE__).'/includes/');
// Components path.
defined('AGSPP_SERVICES_PATH') || define('AGSPP_SERVICES_PATH', AGSPP_INCLUDES_PATH.'Services/');
// Helpers path.
defined('AGSPP_HELPERS_PATH') || define('AGSPP_HELPERS_PATH', AGSPP_INCLUDES_PATH.'Helpers/');
// Interfaces path.
defined('AGSPP_INTERFACES_PATH') || define('AGSPP_INTERFACES_PATH', AGSPP_INCLUDES_PATH.'Interfaces/');
// Templates path.
defined('AGSPP_TEMPLATES_PATH') || define('AGSPP_TEMPLATES_PATH', AGSPP_PATH.'templates/');
// Log path.
defined('AGSPP_LOG_PATH') || define('AGSPP_LOG_PATH', ABSPATH.'wp-content/agspp-logs/');
// General log file path.
defined('AGSPP_GA_LOG_PATH') || define('AGSPP_GA_LOG_PATH', AGSPP_LOG_PATH.'activity.log');
// User log file path.
defined('AGSPP_USR_LOG_PATH') || define('AGSPP_USR_LOG_PATH', AGSPP_LOG_PATH.'user-{uid}.log');

/**
 * Include load file and load all plugin
 * files.
 */
require_once('load.php');

$AGSPP_USER = new Agspp\Services\User();


add_action('init', function(){
    global $DI_Container, $AGSPP;
    /**
     * Global AGSPP object.
     */
    $AGSPP = $DI_Container->getService('agspp');
}, 15);

/**
 * Run install.
 */
register_activation_hook(__FILE__, 'agspp_install_hook');