<?php

/**
 * Return array of database tables and
 * current plugin table versions.
 *
 * @return array [database tables and their versions]
 */
function agspp_install_databases()
{
    return [
        [
            'table'     => 'portfolio',
            'version'   => 1.0
        ],
        [
            'table'     => 'spot_prices',
            'version'   => 1.0,
            'seed'      => true
        ],
        [
            'table'     => 'wishlist',
            'version'   => 1.0
        ]
    ];
}

/**
 * Seed database tables.
 *
 * @since 1.2
 *
 * @param object $wpdb [WordPress database object]
 * @param string $database_name [database table to seed]
 */
function agspp_install_seed_database($wpdb, $database_name)
{
    /**
     * Seed file.
     */
    $sql_raw    = file_get_contents(AGSPP_INSTALL_PATH."seeds/{$database_name}.sql");
    $sql        = str_replace('{table}', $wpdb->prefix.$database_name, $sql_raw);

    /**
     * Insert into database.
     */
    $wpdb->query($sql);
}

/**
 * Insert/Update default site options.
 *
 * @since 1.2
 */
function agspp_install_options()
{
    /**
     * Add product notice.
     */
    update_option('agspp_before_product_info_text', '<p><span style="color: #9e0303;">Receive</span> <a href="https://agspp.com/shipping-insurance/" target="_blank">F R E E </a><span style="color: #9e0303;">Shipping With Orders $99 or More</span>. <a href="https://www.google.com/search?q=%23AmericanGoldSPP" target="_blank">#AmericanGoldSPP</a></p>');

    /**
     * Add paypal/cc extra charge.
     */
    update_option('agspp_cc_paypal_charge', '0.06');

    /**
     * Add phone number.
     */
    update_option('agspp_phone_number', '+1 (844) 594-0050');

    /**
     * Default pages
     */

    // Account page.
    update_option('agspp_page_user_account', 'account');
    // Login page.
    update_option('agspp_page_login', 'login');
    // Register page.
    update_option('agspp_page_register', 'register');
}

/**
 * Install AGSPP plugin.
 *
 * - Install/update database tables.
 * - Insert/Update options.
 *
 * @since 1.2
 */
function agspp_install()
{
    /**
     * Load WordPress' database file.
     */
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');

    /**
     * Set table option name.
     */
    $table_option_name = 'agspp_tbl_%s_vsn';

    /**
     * Loop through databases and install if
     * version is different from installed
     * version.
     */
    foreach (agspp_install_databases() as $database):

        /**
         * Installed table version.
         */
        $installed_version  = (float) get_option(sprintf($table_option_name, $database['table']));

        /**
         * Local table version.
         */
        $local_version      = (float) $database['version'];

        /**
         * If local version is different then installed
         * version break move onto next table.
         */
        if($local_version === $installed_version):
            continue;
        endif;

        /**
         * Get
         */
        global $wpdb;

        /**
         * SQL
         */
        $sql_raw    = file_get_contents(AGSPP_INSTALL_PATH."databases/{$database['table']}.sql");
        $sql        = str_replace(
            ['{table}', '{charset_collate}'],
            [$wpdb->prefix.$database['table'], $wpdb->get_charset_collate()],
            $sql_raw
        );

        /**
         * Insert table.
         */
        dbDelta( $sql );

        /**
         * Seed database.
         */
        if(isset($database['seed']) && true===$database['seed']):
            agspp_install_seed_database($wpdb, $database['table']);
        endif;

        /**
         * Update table option.
         */
        update_option(sprintf($table_option_name, $database['table']), $database['version']);

    endforeach;

    /**
     * Update options.
     */
    agspp_install_options();
}
