CREATE TABLE `{table}` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `metal` varchar(15) DEFAULT NULL,
  `ask` float DEFAULT NULL,
  `bid` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) {charset_collate}