<?php
/**
 * ServiceLoader
 *
 * Loads services.
 *
 * Created: 02.18.17 @ 09:10 EST
 * Modified: 02.18.17 @ 11:16 EST
 *
 * @version 1.1
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp;

class Di_Container
{
    /**
     * Array of include files.
     *
     * @since 1.0
     *
     * @var array $_files_includes
     */
    private $_files_includes = [];

    /**
     * Array of helper files.
     *
     * @since 1.0
     *
     * @var array $_files_helpers
     */
    private $_files_helpers = [];

    /**
     * Array of interface files.
     *
     * @since 1.0
     *
     * @var array $_files_interfaces
     */
    private $_files_interfaces = [];

    /**
     * Array of service files.
     *
     * @since 1.0
     *
     * @var array $_files_services
     */
    private $_files_services = [];

    /**
     * Array of registered services.
     *
     * @since 1.0
     *
     * @var array $_registered_services
     */
    private $_registered_services = [];

    /**
     * Array of loaded services.
     *
     * @since 1.0
     *
     * @var array $_loaded_services
     */
    private $_loaded_services = [];

    /**
     * Array of dependencies for services.
     *
     * @since 1.0
     *
     * @var array $_service_dependencies;
     */
    private $_service_dependencies = [];

    /**
     * Di_Container constructor.
     * Call WordPress action hooks.
     *
     * @since 1.0
     */
    public function __construct()
    {
        add_action('init', [$this, 'loadRegisteredServices'], 10);
        add_action('init', [$this, 'loadServiceDependencies'], 12);
    }


    /**
     * Load includes file.
     *
     * @since 1.0
     *
     * @param string $name [name for array]
     * @param string $include [file to include]
     *
     * @throws [error if file does not exist]
     */
    public function loadInclude($name, $include)
    {
        if(!file_exists(AGSPP_INCLUDES_PATH."{$include}.php")):
            throw new \Exception("Service file {$include} does not exist!");
        endif;

        /**
         * If file exists put it into the container
         * and include the file.
         */
        $this->_files_includes[$name] = $include;
        include (AGSPP_INCLUDES_PATH."{$include}.php");
    }

    /**
     * Load helper file.
     *
     * @since 1.0
     *
     * @param string $name [name for array]
     * @param string $helper [file to include]
     *
     * @throws [error if file does not exist]
     */
    public function loadHelper($name, $helper)
    {
        if(!file_exists(AGSPP_HELPERS_PATH."{$helper}.php")):
            throw new \Exception("Service file {$helper} does not exist!");
        endif;

        /**
         * If file exists put it into the container
         * and include the file.
         */
        $this->_files_helpers[$name] = $helper;
        include (AGSPP_HELPERS_PATH."{$helper}.php");
    }

    /**
     * Load interface file.
     *
     * @since 1.0
     *
     * @param string $name [name for array]
     * @param string $interface [file to include]
     *
     * @throws [error if file does not exist]
     */
    public function loadInterface($name, $interface)
    {
        if(!file_exists(AGSPP_INTERFACES_PATH."{$interface}.php")):
            throw new \Exception("Service file {$interface} does not exist!");
        endif;

        /**
         * If file exists put it into the container
         * and include the file.
         */
        $this->_files_interfaces[$name] = $interface;
        include (AGSPP_INTERFACES_PATH."{$interface}.php");
    }

    /**
     * Load service file.
     *
     * @since 1.0
     *
     * @param string $name [name for array]
     * @param string $service [file to include]
     *
     * @throws [error if file does not exist]
     */
    public function loadService($name, $service)
    {
        if(!file_exists(AGSPP_SERVICES_PATH."{$service}.php")):
            throw new \Exception("Service file {$service} does not exist!");
        endif;

        /**
         * If file exists put it into the container
         * and include the file.
         */
        $this->_files_services[$name] = $service;
        include (AGSPP_SERVICES_PATH."{$service}.php");
    }

    /**
     * Register service to be loaded.
     *
     * Register service and dependencies.
     *
     * @since 1.0
     *
     * @param string $name [name of service]
     * @param string $service [service class name]
     * @param array $dependencies [service dependencies]
     */
    public function registerService($name, $service, $dependencies=[])
    {
        $this->_registered_services[$name] = $service;
        $this->_service_dependencies[$name] = $dependencies;
    }

    /**
     * Loads files for all registered services.
     * Additionally, sets class in loaded
     * service array.
     *
     * @since 1.0
     */
    public function loadRegisteredServices()
    {
        foreach($this->_registered_services as $name => $service):
            if(!class_exists($service)):
                throw new \Exception("'{$service}' class does not exists.");
            endif;
            $this->_loaded_services[$name] = new $service();
        endforeach;
    }

    /**
     * Load service dependencies.
     *
     * @since 1.0
     */
    public function loadServiceDependencies()
    {
        /**
         * Loop through loaded services and set
         * necessary dependencies.
         */
        foreach($this->_loaded_services as $name => $service):
            if(!empty($this->_service_dependencies[$name])):
                $this->_loaded_services[$name]->set_dependencies($this->_findDependencies($name));
            endif;
        endforeach;
    }

    /**
     * Gets service object.
     *
     * @since 1.0
     *
     * @param string $service_name [name of service to get.]
     *
     * @return mixed|null [Service object]
     *
     * @throws \Exception [if no service is loaded]
     */
    public function getService($service_name)
    {
        if(!array_key_exists($service_name, $this->_loaded_services)):
            throw new \Exception("Requested service '{$service_name}' is not registered!");
        endif;
        return $this->_loaded_services[$service_name];
    }

    /**
     * List all helpers.
     *
     * @return array [all loaded helpers]
     */
    public function showAllIncludes()
    {
        return $this->_files_includes;
    }

    /**
     * List all helpers.
     *
     * @return array [all loaded helpers]
     */
    public function showAllHelpers()
    {
        return $this->_files_helpers;
    }

    /**
     * List all interfaces.
     *
     * @return array [all loaded interfaces]
     */
    public function showAllInterfaces()
    {
        return $this->_files_interfaces;
    }

    /**
     * List all registered services.
     *
     * @return array [all loaded services]
     */
    public function showAllServices()
    {
        return $this->_loaded_services;
    }

    /**
     * Find dependencies for service and return
     * them in an array.
     *
     * @since 1.0
     *
     * @param string $service_name [name of service]
     * @return array [dependencies for service]
     */
    private function _findDependencies($service_name)
    {
        $dependencies = [];

        foreach ($this->_service_dependencies[$service_name] as $deps):
            $dependencies[$deps] = $this->_loaded_services[$deps];
        endforeach;

        return $dependencies;
    }

    /**
     * Magic method to return service.
     *
     * @param string $service_name [name of service to get]
     *
     * @return object [service object]
     */
    public function __get($service_name)
    {
        return $this->getService($service_name);
    }
}