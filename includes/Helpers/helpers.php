<?php


/**
 * agspp_sib_user_edit
 * 
 * Edit/Create user with Sendinblue email platform.
 * 
 * @since 1.0
 * 
 * @param (string) $email_address [user email]
 * @param (string) $first_name [user first name]
 * @param (string) $last_name [user last name]
 * @param (array) $lists_add [list ids to add user to]
 * @param (array) $lists_del [list ids to remove user from]
 * 
 * @return (array) [Sendinblue api response]
 */
function agspp_sib_user_update($email_address='',$first_name='',$last_name='',$lists_add=[10],$lists_del=[])
{    
    global $AGSPP_USER;
    /**
     * If no email, first name or
     * last name then stop.
     */
    if(''===$email_address):
        return;
    endif;
    
    /**
     * Sendinblue data array.
     */
    $sendinblue_data = [
        'email' => $email_address,
        'attributes' => [
            'FIRST_NAME' => $first_name,
            'LAST_NAME' => $last_name
        ],
        'listid' => $lists_add,
        'listid_unlink' => $lists_del
    ];
    
    /**
     * Add user to the mailing list and 
     * update his WP meta.
     */
    $mailin     = new Agspp\Services\Sendinblue("https://api.sendinblue.com/v2.0","1mh04xSWswjQfJaV");
    $results    = $mailin->create_update_user($sendinblue_data);
    
    if('success'===$results['code']):        
        $AGSPP_USER->update_meta('_sib_id', $results['data']['id']);
        $AGSPP_USER->update_meta('_sib_lists', $lists_add);
        /**
         * Log user's activity
         */
        $user_log   = str_replace('{uid}', $AGSPP_USER->id, AGSPP_USR_LOG_PATH);
        _log($user_log, "'{$email_address}' was added to Sendinblue mailing list.");
        /**
         * Log general activity
         */
        _log(AGSPP_GA_LOG_PATH, "'{$email_address}' was added to Sendinblue mailing list.");
    endif; 
    
    return $results;
}


/**
 * agspp_user_login_callback
 * 
 * Log user in.
 * 
 * @since 1.0
 * 
 * @param (object) $form [form passed from processor]
 */
function agspp_user_login_callback($form)
{
    global $AGSPP_USER;    
    
    /**
     * Verify reCaptcha.
     */
    if(!_verify_recaptcha()) :
        $form->set_error('message', 'Invalid reCaptcha.');
        return;
    endif;
  
    /**
     * Try to log user in.
     */       
    if(!$AGSPP_USER->login()):
        $form->set_error('message','Incorrect login credentials!');
        return;
    endif;
    
    /**
     * Redirect user.
     */
    wp_redirect(agspp_get_redirect_url());
    exit();
}


/**
 * agspp_user_register_callback
 * 
 * Process register.
 * 
 * @since 1.0
 * 
 * @param (object) $form [form passed from processor]
 */
function agspp_user_register_callback($form)
{
    global $AGSPP_USER;
    
    /**
     * Verify reCaptcha.
     */
    if(!_verify_recaptcha()) :
        $form->set_error('message', 'Invalid reCaptcha.');
        return;
    endif;
    
    /**
     * If email exists then return error.
     */
    if($AGSPP_USER->get_by('email', 'email')):
        $form->set_error('message', sprintf('This email is already registered! <a href="%s">Please click here to login.</a>',site_url('login')));
        return;
    endif;

    /**
     * Insert user into database and add
     * ip address.
     */
    $AGSPP_USER->add();
    $AGSPP_USER->update_meta('_register_ip', agspp_ip());
    $AGSPP_USER->update_meta('_password_set_on', current_time('timestamp'));
    $AGSPP_USER->update_meta('_used_passwords', [wp_hash_password(_field('password'))]);

    /**
     * Subscribe user to list(s)
     */
    if('on'===_field('email_signup')):        
        agspp_sib_user_update(_field('email'), _field('first_name'), _field('last_name'), agspp_get_sib_lists_add());    
    endif;

    /**
     * Setup email data and send email.
     */
    $data = [
        'user_login'    => _field('email'),
        'blogname'      => get_option('blogname'),
        'email_heading' => 'Welcome to American Gold | SPP'
    ];

    _email(_field('email'), 'Your account on American Gold | SPP', $data, 'emails/customer-new-account.php');
    
    /**
     * If user can login in, do so,
     * send welcome email and 
     * redirect.
     */
    if($AGSPP_USER->login()):       
        wp_redirect(agspp_get_redirect_url());
        exit();
    endif;        
}

/**
 * agspp_user_pass_forgot_callback
 * 
 * Process forgot password.
 * 
 * @since 1.0
 * 
 * @param (object) $form [form passed from processor]
 */
function agspp_user_pass_forgot_callback($form)
{
    global $AGSPP_USER;
    
    /**
     * Verify reCaptcha.
     */
    if(!_verify_recaptcha()) :
        $form->set_error('invalid-captcha', 'Invalid reCaptcha.');
        return;
    endif;
    
    /**
     * Set user.
     */
    $user = get_user_by('email', _field('email')) ? get_user_by('email', _field('email')) : get_user_by('login', _field('email'));
    $AGSPP_USER->set($user);
    
    if($AGSPP_USER->id > 0):
        /**
         * Generate password reset key
         * and update user's meta.
         */
        $key = md5(microtime());
        $AGSPP_USER->update_meta('_reset_password_key', $key);
        $AGSPP_USER->update_meta('_reset_password_time', current_time('timestamp'));

        /**
         * Setup email data and send email.
         */
        $data = [
            'user_login'    => _field('email'),
            'email'         => _field('email'),
            'blogname'      => get_option('blogname'),
            'email_heading' => 'Password Reset',
            'reset_key'     => $key
        ];

        _email($AGSPP_USER->user->user_email, 'Reset Your Password [AGSPP]', $data, 'emails/customer-reset-password.php');

        /**
         * Log user's activity
         */
        $user_log   = str_replace('{uid}', $AGSPP_USER->id, AGSPP_USR_LOG_PATH);
        _log($user_log, "'"._field('email')."' requested to reset his/her password.");
    endif;

    /**
     * Log general activity
     */
    _log(AGSPP_GA_LOG_PATH, "'"._field('email')."' requested to reset his/her password.");
    
    /**
     * Set message or user.
     */
    $form->set_error('message', 'Please check your email for a link to reset your password! The link will expire in 3 hours. Please note, it may have gone into your junk/spam folder.');
}

/**
 * agspp_user_pass_reset_callback
 * 
 * Process forgot password.
 * 
 * @since 1.0
 * 
 * @param (object) $form [form passed from processor]
 */
function agspp_user_pass_reset_callback($form)
{
    global $AGSPP_USER;

    /**
     * Get used passwords.
     */
    $used_passwords     = $AGSPP_USER->meta('_used_passwords') ? $AGSPP_USER->meta('_used_passwords') : [];

    /**
     *  See if user has already
     *  used this password. If so do not
     *  let him use it again.
     */
    if(true === agspp_check_used_passwords($used_passwords, _field('password'))):
        $form->set_error('error', 'You have used this password before, please choose a different one!');
        return;
    endif;

    /**
     * Add new password to used
     * passwords array.
     */
    $used_passwords[] = wp_hash_password(_field('password'));

    /**
     * Process and reset password.
     */
    $AGSPP_USER->update_meta('_password_set_on', current_time('timestamp'));
    $AGSPP_USER->update_meta('_used_passwords', $used_passwords);
    $AGSPP_USER->update_meta('_reset_password_key', '');
    $AGSPP_USER->update_password();

    /**
     * Log user's activity
     */
    $user_log   = str_replace('{uid}', $AGSPP_USER->id, AGSPP_USR_LOG_PATH);
    _log($user_log, "'{$AGSPP_USER->user->user_login}' successfully reset his/her password.");

    /**
     * Log general activity
     */
    _log(AGSPP_GA_LOG_PATH, "'{$AGSPP_USER->user->user_login}' successfully reset his/her password.");

    /**
     * Log user in and redirect to my-account page.
     */
    $email = (''!==_get('email', FILTER_SANITIZE_EMAIL)) ? _get('email', FILTER_SANITIZE_EMAIL) : $AGSPP_USER->user->user_email;

    if($AGSPP_USER->login($email, _field('password'))):
        wp_redirect(agspp_get_redirect_url());
        exit();
    endif;
}


/**
 * agspp_sib_signup_callback
 * 
 * Process sib email signup.
 * 
 * @since 1.0
 * 
 * @param (object) $form [form passed from processor]
 */
function agspp_sib_signup_callback($form)
{    
    /**
     * Verify reCaptcha.
     */
    if(!_verify_recaptcha()) :
        $form->set_error('invalid-captcha', 'Invalid reCaptcha.');
        return;
    endif;
    
    agspp_sib_user_update(_field('email'), _field('first_name'), _field('last_name'), agspp_get_sib_lists_add());
    
    $form->set_error('message', 'Thank you for signing up for our email.');
}


#########################################
### 
### Form field functions.
###
#########################################

/**
 * agspp_get_field
 * 
 * Get specific post field.
 * 
 * @since 1.0
 * 
 * @param (string) $field [name of field to get]
 * 
 * @return (string) [cleaned field value] 
 */
function _field($field='')
{
    return isset(agspp_user_get_post_fields()[$field]) ? agspp_user_get_post_fields()[$field] : '';
}

/**
 * _get
 *
 * Get's and cleans specified get variable.
 *
 * @since 1.0
 *
 * @param (string) $field [name of field to get and clean]
 * @param (const) $filter [PHP input filter]
 * @return (mixed) [value of requested get variable]
 */
function _get($field='', $filter=FILTER_SANITIZE_STRING)
{
    $var    = isset($_GET[$field]) ? urldecode($_GET[$field]) : '';
    return filter_var($var, $filter);
}


/**
 * agspp_get_sib_lists_add
 * 
 * Gets and cleans "add" list ids
 * to add user to theses lists.
 * 
 * @return (array) [list ids]
 */
function agspp_get_sib_lists_add()
{
    $lists  = preg_replace('/[^0-9-]/i', '', filter_input(INPUT_POST, 'sib_lists_add', FILTER_SANITIZE_STRING));
    return (''!==$lists) ? explode(',',$lists) : null;
}


/**
 * agspp_get_sib_lists_del
 * 
 * Gets and cleans "delete" list ids
 * to remove user from theses lists.
 * 
 * @return (array) [list ids]
 */
function agspp_get_sib_lists_del()
{   
    $lists  = preg_replace('/[^0-9-]/i', '', filter_input(INPUT_POST, 'sib_lists_del', FILTER_SANITIZE_STRING)); 
    return (''!==$lists) ? explode(',',$lists) : null;
}


/**
 * agspp_user_get_post_fields
 * 
 * Gets and cleans form post 
 * fields.
 * 
 * @since 1.0
 * 
 * @return (array) [cleaned fields]
 */
function agspp_user_get_post_fields()
{
    return [
        'first_name'            => filter_input(INPUT_POST, 'first_name', FILTER_SANITIZE_STRING),
        'last_name'             => filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_STRING),
        'email'                 => filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING),       
        'password'              => filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING),      
        'email_signup'          => filter_input(INPUT_POST, 'email_signup', FILTER_SANITIZE_STRING),
        'remember'              => filter_input(INPUT_POST, 'remember', FILTER_SANITIZE_STRING),
        'billing_first_name'    => filter_input(INPUT_POST, '_billing_first_name', FILTER_SANITIZE_STRING),            
        'billing_last_name'     => filter_input(INPUT_POST, '_billing_last_name', FILTER_SANITIZE_STRING),            
        'billing_company'       => filter_input(INPUT_POST, '_billing_company', FILTER_SANITIZE_STRING),            
        'billing_address_1'     => filter_input(INPUT_POST, '_billing_address_1', FILTER_SANITIZE_STRING),            
        'billing_address_2'     => filter_input(INPUT_POST, '_billing_address_2', FILTER_SANITIZE_STRING),            
        'billing_city'          => filter_input(INPUT_POST, '_billing_city', FILTER_SANITIZE_STRING),            
        'billing_postcode'      => filter_input(INPUT_POST, '_billing_postcode', FILTER_SANITIZE_STRING),            
        'billing_country'       => filter_input(INPUT_POST, '_billing_country', FILTER_SANITIZE_STRING),            
        'billing_state'         => filter_input(INPUT_POST, '_billing_state', FILTER_SANITIZE_STRING),           
        'billing_email'         => filter_input(INPUT_POST, '_billing_email', FILTER_SANITIZE_STRING),           
        'billing_phone'         => filter_input(INPUT_POST, '_billing_phone', FILTER_SANITIZE_STRING),            
        'shipping_first_name'   => filter_input(INPUT_POST, '_shipping_first_name', FILTER_SANITIZE_STRING),            
        'shipping_last_name'    => filter_input(INPUT_POST, '_shipping_last_name', FILTER_SANITIZE_STRING),            
        'shipping_company'      => filter_input(INPUT_POST, '_shipping_company', FILTER_SANITIZE_STRING),            
        'shipping_address_1'    => filter_input(INPUT_POST, '_shipping_address_1', FILTER_SANITIZE_STRING),            
        'shipping_address_2'    => filter_input(INPUT_POST, '_shipping_address_2', FILTER_SANITIZE_STRING),            
        'shipping_city'         => filter_input(INPUT_POST, '_shipping_city', FILTER_SANITIZE_STRING),            
        'shipping_postcode'     => filter_input(INPUT_POST, '_shipping_postcode', FILTER_SANITIZE_STRING),            
        'shipping_country'      => filter_input(INPUT_POST, '_shipping_country',FILTER_SANITIZE_STRING),            
        'shipping_state'        => filter_input(INPUT_POST, '_shipping_state', FILTER_SANITIZE_STRING)
   ];
}


/**
 * agspp_manual_user_meta
 * 
 * User meta for users added through 
 * Woocommerce manual orders.
 * 
 * @since 1.0
 * 
 * @return (array) [user meta for manually added users]
 */
function agspp_manual_user_meta()
{
    return [
        'billing_first_name',
        'billing_last_name',
        'billing_company',
        'billing_address_1',
        'billing_address_2',
        'billing_city',
        'billing_postcode',
        'billing_country',
        'billing_state',
        'billing_email',
        'billing_phone',
        'shipping_first_name',
        'shipping_last_name',
        'shipping_company',
        'shipping_address_1',
        'shipping_address_2',
        'shipping_city',
        'shipping_postcode',
        'shipping_country',
        'shipping_state',
    ];
}


#########################################
### 
### Price functions.
###
#########################################


/**
 * agspp_get_metal_item_price
 * 
 * Returns price of item based on 
 * spot price and quantity.
 * 
 * @param (int)  $product_id [product id to get price for]
 * @param (int) $quantity [quantity of product in cart]
 * @param (float) $price [original product price]
 * 
 * @return (float) [product price]
 */
function agspp_get_metal_item_price($product_id,$quantity,$price)
{
    /**
     * Get product metal, weight, and 
     * spot price and bulk price.
     */
    $product_metal  = get_post_meta($product_id, '_product_metal', 1);
    $spot_price     = (float) get_option('spot_price_'.$product_metal);
    $product_weight = (float) get_post_meta($product_id, '_product_weight', 1);
    $bulk_price     = agspp_get_price_in_range($quantity, get_post_meta($product_id, '_bulk_pricing', 1), $price);    
    /**
     * Return price calculation.
     */
    return (float) round((($spot_price*$product_weight) + $bulk_price), 2);
}


/**
 * agspp_get_price_in_range
 * 
 * Returns the product price of product with
 * bulk pricing.
 * 
 * @since 1.0
 * 
 * @param (int) $qty [quantity of products(1) in cart]
 * @param (array) $ranges [bulk pricing ranges]
 * @param (float) $price [original product price]
 * 
 * @return (float) [product price]
 */
function agspp_get_price_in_range($qty, $ranges=[], $price)
{       
    foreach ($ranges as $range):
        
        /**
         * Set to and from.
         */
        $from   = (int) $range['from'];
        $to     = (int) $range['to'] != '' ? $range['to'] : 1000000000000000000000000;
        $q 	= (int) $qty;
        
        /**
         * If price is in range return price.
         */

        if($q >= $from && $q <= $to):
            return $range['price'];
        endif;
        
    endforeach;
    
    /**
     * As a fail safe, if nothing was found 
     * in range, return price.
     */
     return $price;
}


#########################################
### 
### Misc. functions.
###
#########################################


/**
 * agspp_get_redirect
 * 
 * Get redirect url from post or 
 * get variable.
 * 
 * @since 1.0
 * 
 * @return (string) [redirect url]
 */
function agspp_get_redirect_url()
{
    /**
     * Get redirect var.
     */
    $redirect = isset($_REQUEST['redirect']) ? urldecode($_REQUEST['redirect']) : false;
    /**
     * If no redirect return false.
     */
    if(!$redirect):
        return false;
    endif;
    
    return filter_var($redirect, FILTER_SANITIZE_URL);
}


/**
 * agspp_random_password
 * 
 * Generates a random password.
 * 
 * @since 1.0
 * 
 * @param (int) $length [length of password]
 * 
 * @return (string) [random password]
 */
function agspp_random_password($length=12)
{
    /**
     * List of characters for password.
     */ 
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_=+;|:?<>";
    
    /**
     * Shuffle string and return desired 
     * length.
     */
    return substr(str_shuffle($chars), 0, $length);
}


/**
 * agspp_verify_password_reset_link
 * 
 * Verifies that the password 
 * reset link is valid.
 * 
 * @since 1.0
 */
function agspp_verify_password_reset_link()
{
    $status     = 'fail';    
    /**
     * Email and key.
     */
    $email_raw  = isset($_GET['email']) ? urldecode($_GET['email']) : '';
    $email      = filter_var($email_raw, FILTER_SANITIZE_EMAIL);
    $key_raw    = isset($_GET['key']) ? urldecode($_GET['key']) : '';
    $key        = filter_var($key_raw, FILTER_SANITIZE_EMAIL);       

    /**
     * See if user exists or key is correct.
     * Also verify the link time is less
     * than 3 hours.
     */
    $user       = get_user_by('email', $email);
    
    if($user && $key===get_user_meta($user->ID, '_reset_password_key', 1)):

        $user_time  = get_user_meta($user->ID, '_reset_password_time', 1);
        $min_3hrs   = strtotime('-3 hours', current_time('timestamp'));

        if($user_time >= $min_3hrs):
            $status = 'success';
        else:
            $status = 'expired';
        endif;
    endif;
    
    /**
     * Return status, message
     * and user.
     */
    return [
        'status'    => $status,
        'user'      => $user ? $user : false
    ];    
}

/**
 * agspp_ip
 * 
 * Get's a user's ip address.
 * 
 * @since 1.0
 * 
 * @return (string) [user's ip]
 */
function agspp_ip()
{
    $ip = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP'])):
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])):
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else:
        $ip = $_SERVER['REMOTE_ADDR'];
    endif;
    
    return $ip;
}

/**
 * _log
 * 
 * Log message.
 * 
 * @since 1.0
 * 
 * @param (string) $path [file to write to]
 * @param (string) $msg [message to write to file]
 */
function _log($path,$msg)
{
    /**
     * Compose log message with
     * date, and message.
     */
    $date       = date('D, F j, Y @ H:i:s', current_time('timestamp'));
    $message    = sprintf("[%s] | IP:%s | %s\n", $date, agspp_ip(), $msg);
    
    /**
     * Log to file.
     */
    file_put_contents($path, $message, FILE_APPEND);
}

/**
 * _verify_recaptcha
 * 
 * Verifies reCaptcha.
 * 
 * @since. 1.0
 */
function _verify_recaptcha()
{
    /**
     * Set variables.
     */
    $secret_key     = get_option('agspp_recaptcha_secret');
    $ip_address     = agspp_ip();
    $response       = filter_input(INPUT_POST, 'g-recaptcha-response', FILTER_SANITIZE_STRING);    
    $url            = sprintf("https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s", $secret_key, $response, $ip_address);

    /**
     * Get response from Google.
     */
    $google_response= wp_remote_get($url);
    
    /**
     * Verify response is array.
     */
    if(!is_array($google_response)):
        return false;
    endif;
    
    /**
     * Decode json response.
     */
    $json_array     = json_decode($google_response['body'], 1);
    
    return $json_array['success'] == 1 ? true : false;    
}

/**
 * _email
 *
 * Emails transactional emails to
 * users.
 *
 * @since 1.1
 *
 * @param (string) $to [email recipient]
 * @param (string) $subject [email subject]
 * @param (string) $data [data to replace in template]
 * @param (string) $template [Woocommerce emial template]
 */
function _email($to, $subject, $data, $template)
{
    global $woocommerce;

    /**
     * Set mailer.
     */
    $mailer = $woocommerce->mailer();
    /**
     * Get email template and create
     * message.
     */
    ob_start();
    wc_get_template($template, $data);
    $message = ob_get_clean();

    /**
     * Send email.
     */
    $mailer->send($to, $subject, $message);
}

/**
 * Run on activate/install hook.
 *
 * This function includes install file
 * and installs the plugin.
 */
function agspp_install_hook()
{
    include AGSPP_INSTALL_PATH.'install.php';
    agspp_install();
}

/**
 * agspp_check_used_passwords
 *
 * Check if user has already used
 * the current password.
 *
 * @since 1.1
 *
 * @param (string) $new_password [new raw password]
 * @param (array) $used_passwords [array of used passwords.]
 *
 * @return (bool) [true if new password was previously used.]
 */
function agspp_check_used_passwords($used_passwords=[], $new_password='')
{
    foreach($used_passwords as $password):
        if(true === wp_check_password($new_password, $password)):
            return true;
        endif;
    endforeach;

    return false;
}

/**
 * Debug array
 *
 * @param array $var
 */
function _d($var=[])
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

/**
 * Debug array
 *
 * @param array $var
 */
function _dd($var=[])
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die();
}

