<?php


/**
 * agspp_short_code_form_login
 * 
 * Shortcode to return and process 
 * user login.
 * 
 * @since 1.0
 * 
 * @param type $attributes [attributes passed from the shortcode]
 */
function agspp_short_code_form_login($attributes=[])
{
     /**
     * Set default attrs
     */
    $atts = shortcode_atts([        
        'redirect'   => get_permalink()
    ], $attributes);
    
    /**
     * Set redirect url and get form.
     */
    global $AGSPP;       
    $redirect_url = agspp_get_redirect_url() ? agspp_get_redirect_url() : $atts['redirect'];
    
    /**
     * Include form.
     */
    include AGSPP_TEMPLATES_PATH.'agspp/forms/login.html.php';
}
add_shortcode('login','agspp_short_code_form_login');


/**
 * agspp_short_code_form_register
 * 
 * Shortcode to return and process 
 * user registration.
 * 
 * @since 1.0
 * 
 * @param type $attributes [attributes passed from the shortcode]
 */
function agspp_short_code_form_register($attributes=[])
{
     /**
     * Set default attrs
     */
    $atts = shortcode_atts([        
        'redirect'   => get_permalink(),
        'lists'      => '',
    ], $attributes);    
    
    /**
     * Set redirect url, and get form.
     */
    global $AGSPP;
    $redirect_url = agspp_get_redirect_url() ? agspp_get_redirect_url() : $atts['redirect']; 
    
    /**
     * Include form.
     */
    include AGSPP_TEMPLATES_PATH.'agspp/forms/register.html.php';
}
add_shortcode('register','agspp_short_code_form_register');


/**
 * agspp_short_code_form_password_forgot
 * 
 * Shortcode to return and process 
 * user's forgot password form.
 * 
 * @since 1.0
 * 
 * @param type $attributes [attributes passed from the shortcode]
 */
function agspp_short_code_form_password_forgot($attributes=[])
{
     /**
     * Set default attrs
     */
    $atts = shortcode_atts([        
        'redirect'   => get_permalink()
    ], $attributes);    
    
    /**
     * Set redirect url, and get form.
     */
    global $AGSPP; 
    $redirect_url = agspp_get_redirect_url() ? agspp_get_redirect_url() : $atts['redirect'];
    
    /**
     * Include form.
     */
    include AGSPP_TEMPLATES_PATH.'agspp/forms/password-forgot.html.php';
}
add_shortcode('password-forgot','agspp_short_code_form_password_forgot');


/**
 * agspp_short_code_form_password_reset
 * 
 * Shortcode to return and process 
 * user's reset password form.
 * 
 * @since 1.0
 * 
 * @param type $attributes [attributes passed from the shortcode]
 */
function agspp_short_code_form_password_reset($attributes=[])
{
     /**
     * Set default attrs
     */
    $atts = shortcode_atts([        
        'redirect'   => site_url(get_option('agspp_user_account_page'))
    ], $attributes);   
    
    /**
     * Set redirect url, and get form.
     */
    global $AGSPP , $AGSPP_USER;
    
    $redirect_url = agspp_get_redirect_url() ? agspp_get_redirect_url() : $atts['redirect'];    
    
    /**
     * Include form.
     */
    if($AGSPP_USER->id > 0):
        include AGSPP_TEMPLATES_PATH.'agspp/forms/password-reset.html.php';
    else:
        return 'This link has expired or is invalid.';
    endif;
}
add_shortcode('password-reset','agspp_short_code_form_password_reset');


/**
 * agspp_short_code_form_email_signup
 * 
 * Shortcode to return and process 
 * email signup form.
 * 
 * @since 1.0
 * 
 * @param type $attributes [attributes passed from the shortcode]
 */
function agspp_short_code_form_email_signup($attributes=[])
{
     /**
     * Set default attrs
     */
    $atts = shortcode_atts([        
        'redirect'   => site_url(get_option('agspp_user_account_page')),
        'lists'      => 10,
    ], $attributes);   
    
    /**
     * Set redirect url, and get form.
     */
    global $AGSPP;
    
    $redirect_url = agspp_get_redirect_url() ? agspp_get_redirect_url() : $atts['redirect'];    
    
    include AGSPP_TEMPLATES_PATH.'agspp/forms/email-signup.html.php';
}
add_shortcode('email-signup','agspp_short_code_form_email_signup');


/**
 * agspp_short_code_form_email_signup_ajax
 * 
 * Shortcode to ajax email signup form.
 * 
 * @since 1.0
 */
function agspp_short_code_form_email_signup_ajax()
{    
    $html  = '<div class="agspp-ajax-email-signup">';
    $html .= '<label for="ajax-email-signup-input">Sign up for our emails</label>';
    $html .= '<input type="text" id="ajax-email-signup-input" class="ajax-email-signup-input" style="padding:6px;"/>';
    $html .= '<button class="kad-btn kad-btn-primary sm-kad-btn ajax-email-button" data-url="'.admin_url('admin-ajax.php').'">Sign up!';
    $html .= '</div>';
    return $html;
}
add_shortcode('email-signup-ajax','agspp_short_code_form_email_signup_ajax');


/**
 * agspp_shortcode_spot_price
 * 
 * Add spot prices shortcode
 * 
 * @since 1.0
 * 
 * @param (array) $attributes [attributes passed from caller]
 */
function agspp_shortcode_spot_prices($attributes=[])
{
    /**
     * Set default attrs
     */
    $atts = shortcode_atts([        
        'tag'   => 'div'
    ], $attributes);   
    
    $ajax_url = admin_url('admin-ajax.php');
    
    /**
     * Return HTML
     */
    return "<{$atts['tag']} class='agspp-spot-prices' data-url='{$ajax_url}'>GOLD <span class='spot-price-gold spot-price'></span> SILVER <span class='spot-price-silver spot-price'></span> PLATINUM <span class='spot-price-platinum spot-price'></span> PALLADIUM <span class='spot-price-palladium spot-price'></span></{$atts['tag']}>";
}
add_shortcode('spot-prices','agspp_shortcode_spot_prices');
