<?php

/**
 * agspp_login_form_rules
 * 
 * Returns array or rules and 
 * messages for the login 
 * form.
 * 
 * @since 1.0
 * 
 * @return array [form rules]
 */
function agspp_login_form_rules()
{
    return [
        'email' => [
            'rules' => [
                'required' => true
            ],
            'messages' => [
                'required' => 'Please enter your email!'
            ]
        ],        
        'password' => [
            'rules' => [
                'required' => true
            ],
            'messages' => [
                'required' => 'Please enter your password!'
            ]
        ],
        'color' => [
            'rules' => [
                'is' => ''
            ]
        ]
    ];
}


/**
 * agspp_regsiter_form_rules
 * 
 * Returns array or rules and 
 * messages for the register 
 * form.
 * 
 * @since 1.0
 * 
 * @return array [form rules]
 */
function agspp_register_form_rules()
{
    return [
        'first_name' => [
            'rules' => [
                'required' => true
            ],
            'messages' => [
                'required'=>'Please enter your first name!'
            ]
        ],   
        'last_name' => [
            'rules' => [
                'required' => true
            ],
            'messages' => [
                'required'=>'Please enter your first name!'
            ]
        ],   
        'email' => [
            'rules' => [
                'required' => true,
                'email'    => true,
                'match'    => 'email_confirm'
            ],
            'messages' => [
                'required'=>'Please enter your email!',
                'match' => 'Your email addresses do not match!'
            ],
        ],    
        'email_confirm' => [
            'rules' => [
                'required' => true
            ],
            'messages' => [
                'required'=>'Please confirm your email!'
            ],
        ],    
        'password' => [
            'rules' => [
                'required' => true,
                'password' => true,
                'match'    => 'password_confirm'
            ],
            'messages' => [
                'required'=>'Please enter your password!',
                'match' => 'Your passwords do not match!'
            ]
        ],
        'password_confirm' => [
            'rules' => [
                'required' => true                
            ],
            'messages' => [
                'required'=>'Please confirm your password!'
            ]
        ],
        'color' => [
            'rules' => [
                'is' => ''
            ]
        ]
    ]; 
}

/**
 * agspp_register_form_rules
 * 
 * Returns array or rules and 
 * messages for the password 
 * forgot form.
 * 
 * @since 1.0
 * 
 * @return array [form rules]
 */
function agspp_pass_forgot_form_rules()
{
    return [
        'email' => [
            'rules' => [
                'required' => true
            ],
            'messages' => [
                'required' => 'Please enter your email!'
            ]
        ],
        'color' => [
            'rules' => [
                'is' => ''
            ]
        ]
    ];
}

/**
 * agspp_pass_reset_form_rules
 * 
 * Returns array or rules and 
 * messages for the password 
 * reset form.
 * 
 * @since 1.0
 * 
 * @return array [form rules]
 */
function agspp_pass_reset_form_rules()
{
    return [
        'password' => [
            'rules' => [
                'required' => true,
                'password' => true,
                'match'    => 'password_confirm'
            ],
            'messages' => [
                'required' => 'Please enter your password!',
                'match'    => 'Your passwords do not macth!'
            ]
        ],
        'password_confirm' => [],
        'color' => [
            'rules' => [
                'is' => ''
            ]
        ]
    ];
}

/**
 * agspp_sib_signup_form_rules
 * 
 * Returns array or rules and 
 * messages for the sib signup 
 * form.
 * 
 * @since 1.0
 * 
 * @return array [form rules]
 */
function agspp_sib_signup_form_rules()
{
    return [
        'first_name' => [
            'rules' => [
                'required' => true
            ],
            'messages' => [
                'required'=>'Please enter your first name!'
            ]
        ],   
        'last_name' => [
            'rules' => [
                'required' => true
            ],
            'messages' => [
                'required'=>'Please enter your first name!'
            ]
        ],   
        'email' => [
            'rules' => [
                'required' => true,
                'email'    => true,
                'match'    => 'email_confirm'
            ],
            'messages' => [
                'required'=>'Please enter your email!',
                'match' => 'Your email addresses do not match!'
            ],
        ],    
        'email_confirm' => [],
        'color' => [
            'rules' => [
                'is' => ''
            ]
        ]
    ];
}

/**
 * agspp_process_forms
 * 
 * Processes site forms.
 * 
 * @since 1.0
 * 
 */
function agspp_process_forms()
{
    global $DI_Container;
    /**
     * Verify nonce
     */
    if(isset($_POST['agspp_form_nonce']) && !wp_verify_nonce($_POST['agspp_form_nonce'], 'agspp_form_nonce')):
        die('No CSRF!');
    endif;

    /**
     * Get Form.
     */
    $form   = $DI_Container->getService('form');

    /**
     * If agspp form not sent 
     * the stop.
     */
    if(!isset($_REQUEST['agspp-form'])):
        return $form;
    endif;
    
    /**
     * Determine which form to 
     * process.
     */
    switch ($_REQUEST['agspp-form']):
        
        case 'login':
            $form->set_fields(agspp_login_form_rules());
            $form->process('agspp_user_login_callback');
            break;
        
        case 'register':
            $form->set_fields(agspp_register_form_rules());
            $form->process('agspp_user_register_callback');
            break;
        
        case 'password-forgot':
            $form->set_fields(agspp_pass_forgot_form_rules());
            $form->process('agspp_user_pass_forgot_callback');
            break;
        
        case 'password-reset':
            $form->set_fields(agspp_pass_reset_form_rules());
            $form->process('agspp_user_pass_reset_callback');
            break;
        
        case 'sib-signup':
            $form->set_fields(agspp_sib_signup_form_rules());
            $form->process('agspp_sib_signup_callback');
            break;
        

    endswitch;

    /**
     * Return form.
     */   
    return $form;
}