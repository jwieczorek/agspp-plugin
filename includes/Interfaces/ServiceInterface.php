<?php

/**
 * Created by PhpStorm.
 * User: joshuawieczorek
 * Date: 2/18/17
 * Time: 11:16 AM
 */

namespace Agspp\Interfaces;

interface ServiceInterface
{
    function set_dependencies($dependencies);
    function wp_action_wp();
}