<?php
/**
 * ServiceBase
 *
 * Base class for all of AGSPP
 * classes.
 *
 * @version 1.0
 *
 * Author: Joshua Wieczorek
 * Created: 2/16/17 @ 11:57.
 * Updated: 2/16/17 @ 11:57.
 */

namespace Agspp;

use Agspp\Services\Sessions;
use Agspp\Services\Html;

class ServiceBase
{
    /**
     * Session class
     *
     * @since 1.0
     *
     * @var object $_session
     */
    protected $_session;

    /**
     * Html class
     *
     * @since 1.0
     *
     * @var object $_html
     */
    protected $_html;

    /**
     * WordPress database object.
     *
     * @since 1.0
     *
     * @var object $_db
     */
    protected $_db;

    /**
     * Custom User object.
     *
     * @since 1.0
     *
     * @var object $_user
     */
    protected $_user;

    /**
     * My account page.
     *
     * @since 1.0
     * @access private
     *
     * @var (string) $_account_page
     */
    protected $_page_account;

    /**
     * Login page.
     *
     * @since 1.0
     * @access private
     *
     * @var (string) $_login_page
     */
    protected $_page_login;

    /**
     * Register page.
     *
     * @since 1.0
     * @access private
     *
     * @var (string) $_register_page
     */
    protected $_page_register;

    /**
     * Page slug to redirect logging
     * out users.
     *
     * @var (string) $_logout_redirect
     */
    protected $_logout_redirect;

    /**
     * Base constructor.
     */
    public function __construct()
    {
        global $AGSPP_USER, $wpdb;

        /**
         * Set session.
         */
        $this->_session = new Sessions();

        /**
         * Set Html creator.
         */
        $this->_html    = new Html();

        /**
         * Set global user object.
         */
        $this->_user    = $AGSPP_USER;

        /**
         * Set WordPress database object.
         */
        $this->_db      = $wpdb;

        /**
         * Set my account page slug.
         */
        $this->_page_account = get_option('agspp_page_user_account');

        /**
         * Set login page slug.
         */
        $this->_page_login = get_option('agspp_page_login');

        /**
         * Set register page slug.
         */
        $this->_page_register = get_option('agspp_page_register');

        /**
         * Set logout redirect slug.
         */
        $this->_logout_redirect = $this->_page_login;
    }
}