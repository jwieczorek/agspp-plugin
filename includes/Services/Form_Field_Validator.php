<?php
/**
 * Form_Field_Validator
 * 
 * Validates form fields.
 * 
 * Created: 02.03.17 @ 18:12 EST
 * Modified: 02.04.17 @ 14:25 EST
 * 
 * @version 1.0
 * 
 * @package    JWieczorek
 * @subpackage Form
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 */

namespace Agspp\Services;

class Form_Field_Validator
{    
    /**
     * required
     * 
     * Validate required field.
     * 
     * If field is empty set form error.
     * 
     * @since 1.0
     * @access public
     * 
     * @param  (string) $name [name of form field to validate]
     * @param  (object) $form [form object]
     * @param  (array) $field_array [field array of rules and messages]
     */
    public function required($name,$form,$field_array)
    {
        if( $form->field($name) == '') :
            $form->set_error($name, $this->_msg($field_array,'required','Required Field!'));
        endif;
    }

    /**
     * minlen
     * 
     * Validate minimum length of field.
     * 
     * If field length is too short set form
     * error.
     * 
     * @since 1.0
     * @access public
     * 
     * @param  (string) $name [name of form field to validate]
     * @param  (object) $form [form object]
     * @param  (array) $field_array [field array of rules and messages]
     */
    public function minlen($name,$form,$field_array)
    {
        $field = $form->field($name);
        $minlen = $field_array['rules']['minlen'];
        if( !isset( $field[($minlen-1)] ) ) :
            $form->set_error($name, $this->_msg($field_array,'minlen',"This field must be at least {$minlen} characters long!"));
        endif;
    }

    /**
     * email
     * 
     * Validate email field.
     * 
     * If field is an email set form
     * error.
     * 
     * @since 1.0
     * @access public
     * 
     * @param  (string) $name [name of form field to validate]
     * @param  (object) $form [form object]
     * @param  (array) $field_array [field array of rules and messages]
     */
    public function email($name,$form,$field_array)
    {
        if(filter_var($form->field($name), FILTER_VALIDATE_EMAIL) === false) :
            $form->set_error($name, $this->_msg($field_array,'email','Please enter a valid email!'));
        endif;
    }

    /**
     * match
     * 
     * Validate match field.
     * 
     * If match does not match desired field
     * set form error.
     * 
     * @since 1.0
     * @access public
     * 
     * @param  (string) $name [name of form field to validate]
     * @param  (object) $form [form object]
     * @param  (array) $field_array [field array of rules and messages]
     */
    public function match($name,$form,$field_array)
    {
        $match = $field_array['rules']['match'];      
        if($form->field($name) != $_REQUEST[$match]) :            
            $form->set_error($name, $this->_msg($field_array,'match',"This field must match {$match}!"));
        endif;
    }

    /**
     * password
     * 
     * Validate password field.
     * 
     * If password is not strong enough
     * set form error.
     * 
     * @since 1.0
     * @access public
     * 
     * @param  (string) $name [name of form field to validate]
     * @param  (object) $form [form object]
     * @param  (array) $field_array [field array of rules and messages]
     */
    public function password($name,$form,$field_array)
    {
        $field = $form->field($name);
        if(!isset($field[7]) || !preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$#", $field)) :
            $form->set_error($name, $this->_msg($field_array,'password','Your password must be at least 8 characters and contain at least 1 symbol, capital letter, number and lowercase letter!'));
        endif;
    }

    /**
     * is
     *
     * Validates field equals value.
     *
     * @since 1.0
     * @access public
     *
     * @param  (string) $name [name of form field to validate]
     * @param  (object) $form [form object]
     * @param  (array) $field_array [field array of rules and messages]
     */
    public function is($name,$form,$field_array)
    {
        $field  = $form->field($name);
        $is     = $field_array['rules']['is'];
        if($field !== $is):
            die('Die you bad robot.');
        endif;
    }

    /**
     * _msg
     * 
     * Return error message.
     * 
     * If field array has error message 
     * then return it, otherwise reuturn
     * default set by calling method.
     * 
     * @since 1.0
     * @access private
     * 
     * @param  (array) $array [field array of data]
     * @param  (string) $msg_name [key of message to get]
     * @param  (string) $default [default if no message was sent]
     * 
     * @return (string) [error message]
     */
    private function _msg($array=[],$msg_name='',$default='')
    {
        return isset($array['messages'][$msg_name]) ? $array['messages'][$msg_name]: $default;
    }
}