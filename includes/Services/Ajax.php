<?php
/**
 * Ajax
 * 
 * Supplies all of the plugins ajax request.
 * 
 * Created: 02.05.17 @ 13:31 EST
 * Modified: 02.17.17 @ 14:42 EST
 * 
 * @version 1.0
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

use Agspp\Interfaces\ServiceInterface;

class Ajax implements ServiceInterface
{        
    /**
     * Spot price object.
     * 
     * @since 1.0
     * @access private
     *
     * @var (object) $_spot_prices
     */
    private $_spot_prices;
    
    /**
     * Portfolio object.
     * 
     * @since 1.0
     * @access private
     *
     * @var (object) $_portfolio
     */
    private $_portfolio;
    
    /**
     * Class constructor.
     * 
     * @since 1.0
     */
    function __construct($config=[])
    {
        $this->_ajax_requests();
    }

    public function set_dependencies($dependencies)
    {
        $this->_spot_prices = $dependencies['spot_price'];
        $this->_portfolio   = $dependencies['portfolio'];
    }

    public function wp_action_wp() {}


    /**
     * Run all ajax requests for this
     * plugin.
     *
     * @since 1.0
     */
    private function _ajax_requests()
    {
        /**
         * Metals portfolio ajax request.
         */
        add_action('wp_ajax_nopriv_portfolio_request', [$this,'ajax_portfolio_metals']);
        add_action('wp_ajax_portfolio_request', [$this,'ajax_portfolio_metals']);
        
        /**
         * Form process ajax.
         */
        add_action('wp_ajax_nopriv_ajax_form', 'agspp_ajax_form');
        add_action('wp_ajax_ajax_form', 'agspp_ajax_form');
        
        /**
         * Spot prices ajax.
         */
        add_action('wp_ajax_nopriv_ajax_spot_prices', [$this,'ajax_spot_prices']);
        add_action('wp_ajax_ajax_spot_prices', [$this,'ajax_spot_prices']);
        
        /**
         * Email signup ajax.
         */
        add_action('wp_ajax_nopriv_ajax_email_signup', [$this,'ajax_email_signup']);
        add_action('wp_ajax_ajax_email_signup', [$this,'ajax_email_signup']);
    }
    
    /**
     * Return user's metal portfolio.
     * 
     * @since 1.0
     * @access public
     */
    public function ajax_portfolio_metals()
    {
        /**
         * Get user id.
         */
        $user_id    = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_NUMBER_INT);
        
        /**
         * Check if user exists. If no user
         * exists return error and die. 
         */
        if(!get_user_by('ID', $user_id)) :
            echo 'error';
            wp_die();
        endif;
        
        /**
         * Metals array.
         */
        $portfolio = $this->_portfolio->get_portfolio($user_id);
        
        /**
         * echo json encoded metals.
         */
        echo json_encode([
            'gold' => $portfolio['gold'] ? $portfolio['gold'] : 0,
            'silver' => $portfolio['silver'] ? $portfolio['silver'] : 0,
            'platinum' => $portfolio['platinum'] ? $portfolio['platinum'] : 0,
            'palladium' => $portfolio['palladium'] ? $portfolio['palladium'] : 0
        ]);
        
        /**
         * Kill WordPress.
         */
        wp_die();
    }
    
    /**
     * ajax_spot_prices
     * 
     * Return json spot prices.
     * 
     * @since 1.0
     * @access public
     */
    public function ajax_spot_prices()
    {
        echo json_encode($this->_spot_prices->spot_prices_array_raw());
        
        /**
         * Kill WordPress.
         */
        wp_die();
    }
    
     /**
     * ajax_email_signup
     * 
     * Sign user up via ajax.
     * 
     * @since 1.0
     * @access public
     */
    public function ajax_email_signup()
    {
        /**
         * Validate email.
         */
        $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        
        if(!$email):
            echo json_encode(['status'=>'fail','message'=>'Please enter a valid email address.']);
            wp_die();
        endif;
        
        /**
         * Insert email address.
         */
        agspp_sib_user_update($email);
        
        /**
         * Return response.
         */
        echo json_encode(['status'=>'success','message'=>'Thank you for signing up for our mailing list.']);
        
        /**
         * Kill WordPress.
         */
        wp_die();
    }
}
