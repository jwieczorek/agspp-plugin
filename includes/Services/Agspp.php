<?php
/**
 * Agspp
 * 
 * Plugin controller class.
 * 
 * Created: 02.05.17 @ 14:29 EST
 * Modified: 02.09.17 @ 08:36 EST
 * 
 * @version 1.0
 * 
 * @package    jwplg
 * @subpackage agspp
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

use Agspp\Interfaces\ServiceInterface;
use Agspp\ServiceBase;

class Agspp extends ServiceBase implements ServiceInterface
{

    /**
     * Plugin's admin class.
     * 
     * @since 1.0
     * @access private
     *
     * @var (object) $_admin
     */
    private $_admin;
    
    /**
     * Global form object.
     * 
     * @since 1.0
     * @access public
     *
     * @var (object) $form
     */
    public $form;
    
    /**
     * Message to display on page 
     * redirect.
     * 
     * @since 1.0
     * @access public
     *
     * @var (string) $redirect_message
     */
    public $redirect_message;
    
    /**
     * Class constructor.
     * 
     * Set variables and run
     * install for this 
     * class.
     * 
     * @since 1.0
     */
    function __construct()
    {
        parent::__construct();

        if(is_admin()):
            $this->_admin       = new Agspp_Admin();
        endif;

        /**
         * Run plugin.
         */
        $this->run();

    }

    function set_dependencies($dependencies)
    {
        $this->form         = $dependencies['form'];
    }


    
    /**
     * _run
     * 
     * Runs plugin's hooks and actions.
     * 
     * @since 1.0
     * @access private
     */
    public function  run()
    {
        /**
         * Run in 'wp' hook.
         */
        add_action('wp', [$this, 'wp_action_wp']);

        /**
         * Run on 'template_redirect' hook.
         */
        add_action('template_redirect', [$this, 'wp_action_template_redirect']);

        /**
         * Run on 'wp_enqueue_scripts' hook.
         */
        add_action('wp_enqueue_scripts', [$this, 'wp_action_wp_enqueue_scripts'] , 999);

        /**
         * Run on 'wp_logout' hook.
         */
        add_action('wp_logout', [$this, 'wp_action_wp_logout']);
        
        /**
         * Run on 'after_setup_theme' hook.
         */
        add_action('after_setup_theme', [$this, 'wp_action_after_setup_theme']);
        
        /**
         * Set email to html.
         */
        add_filter('wp_mail_content_type', [$this,'wp_filter_wp_mail_content_type']);

        /**
         * Change default [email] from name.
         */
        add_filter('wp_mail_from_name', [$this, 'wp_filter_wp_mail_from_name'], 1, 999);

        /**
         * Change default [email] from address.
         */
        add_filter('wp_mail_from', [$this, 'wp_filter_wp_mail_from'], 1, 999);
    }
    
    /**
     * wp_action_wp
     * 
     * Run methods inside WordPress'
     * wp hook.
     * 
     * @since 1.0
     * @access public
     */
    public function wp_action_wp()
    {
        global $pagenow;

         /**
         * Set global form object.
         */
        $this->form     = agspp_process_forms();

        /**
         * Redirect wp-login
         */
        $action = isset($_GET['action']) ? $_GET['action'] : '';

        if('wp-login.php'===$pagenow && 'logout'!==$action):
            wp_redirect(add_query_arg(['redirect'=>urlencode($_GET['redirect_to'])], site_url('login')));
            exit();
        endif;
    }   
    
    /**
     * wp_action_template_redirect
     * 
     * Page redirects.
     * 
     * @since 1.0
     * @access public
     */
    public function wp_action_template_redirect()
    {
        /**
         * If current page is login and user 
         * is already logged in then redirect
         * to account page.
         */
        if(is_page($this->_page_login) && is_user_logged_in()) :
            wp_redirect(site_url($this->_page_account));
            exit();
        endif;
        
        /**
         * If current page is register and user 
         * is already logged in then redirect
         * to account page.
         */
        if(is_page($this->_page_register) && is_user_logged_in()) :
            wp_redirect(site_url($this->_page_account));
            exit();
        endif;
        
        /**
         * If current page is account and user 
         * is not logged in then redirect
         * to login page.
         */
        if(is_page($this->_page_account) && !is_user_logged_in()) :
            wp_redirect(site_url($this->_page_login));
            exit();
        endif;        
    }

    /**
     * wp_action_wp_enqueue_scripts
     *
     * Enqueue and load plugin css and
     * javascripts.
     *
     * @since 1.0
     * @access public
     */
    public function wp_action_wp_enqueue_scripts()
    {
        /**
         * Load Google reCaptcha
         */
        wp_enqueue_script('agspp-recaptcha', 'https://www.google.com/recaptcha/api.js', [], '1', true);

        /**
         * Load High Charts 1 (js)
         */
        wp_enqueue_script('agspp-charts1', 'https://code.highcharts.com/highcharts.js', [], '1', true);


        /**
         * Load High Charts 2 (js)
         */
        wp_enqueue_script('agspp-charts2', 'https://code.highcharts.com/modules/exporting.js', [], '1', true);

        /**
         * Load plugin's js file (js)
         */
        wp_enqueue_script( 'agspp-js', AGSPP_PLUGIN_URL.'assets/js/agspp.js', ['jquery'], '1.0', true);
    }
    
    /**
     * redirect_logout
     * 
     * Redirect user after logout.
     * 
     * @since 1.0
     * @access public
     */
    public function wp_action_wp_logout()
    {
        wp_redirect(site_url($this->_logout_redirect));
        exit;
    }

    /**
     * after_setup_theme
     *
     * Run all after theme setup hooks.
     *
     * @since 1.0
     * @access public
     */
    public function wp_action_after_setup_theme()
    {
        /**
         * Turn off admin bar for non admins.
         */
        if (!current_user_can('administrator') && !is_admin()):
            show_admin_bar(false);
        endif;
    }
    
    /**
     * wp_filter_wp_mail_content_type
     * 
     * Sets email type to 
     * html.
     * 
     * @since 1.0
     * @access public
     */
    public function wp_filter_wp_mail_content_type()
    {
        return "text/html";
    }

    /**
     * wp_email_from_name
     *
     * Changes WordPress' default [email]
     * from name.
     *
     * @return (string) [email from name]
     */
    public function wp_filter_wp_mail_from_name()
    {
        return get_option('blogname');
    }

    /**
     * wp_email_from_address
     *
     * Changes WordPress' default [email]
     * from address.
     *
     * @return (string) [email from name]
     */
    public function wp_filter_wp_mail_from()
    {
        return get_option('admin_email');
    }
}