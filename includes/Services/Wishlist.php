<?php
/**
 * Wishlist
 * 
 * Adds wishlist functionality for users.
 * 
 * Created: 02.05.17 @ 13:33 EST
 * Modified: 02.05.17 @ 13:33 EST
 * 
 * @version 1.1
 *
 * @package    JWieczorek
 * @subpackage agspp
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

use Agspp\Interfaces\ServiceInterface;
use Agspp\ServiceBase;

//use Agspp\Services\Form\Form;

class Wishlist extends ServiceBase implements ServiceInterface
{
    /**
     * Table name for portfolio.
     * 
     * @since 1.0
     * @access private
     *
     * @var (string) $_table
     */
    private $_table;

    /**
     * Class constructor.
     *
     * Set variables and call run
     * method.
     *
     * @since 1.0
     * @access public
     */
    function __construct()
    {
        parent::__construct();

        /**
         * Runs on 'init' hook.
         */
        add_action('wp', [$this,'wp_action_wp']);

        /**
         * Add button to WooCommerce product.
         */
        add_action('woocommerce_after_add_to_cart_button', [$this, 'add_wishlit_link']);
    }

    public function set_dependencies($dependencies) {}

    /**
     * Run on 'wp' hook.
     *
     * Setup class variables and run
     * all necessary actions.
     *
     * @since 1.1
     */
    public function wp_action_wp()
    {
        $this->_table = $this->_db->prefix.'wishlist';

        /**
         * If not add-to-wishlist or no product id return.
         */
        if(!isset($_REQUEST['wishlist'],$_REQUEST['product'])):
            return;
        endif;

        /**
         * Switch actions
         */
        switch ($_REQUEST['wishlist']):
            case 'add':
                $this->add();
                break;
            case 'delete':
                $this->delete();
                break;
        endswitch;
    }
    
    /**
     * Get user's wishlist.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (int) $user_id [user's id]     

     * @return (array) [user's wishlist]
     */
    public function get()
    {        
        $sql = "SELECT * FROM `{$this->_table}` WHERE `user_id`='{$this->_user->id}'";
        return $this->_db->get_results($sql);
    }
    
    /**
     * See if user already has item
     * in their wishlist.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (int) $user_id [user's id]
     * @param (int) $product_id [product id]

     * @return (array/false) [row in user wishlist]
     */
    public function get_single($product_id)
    {
        $sql = "SELECT * FROM `{$this->_table}` WHERE `user_id`='{$this->_user->id}' AND `product_id`='{$product_id}'";
        return $this->_db->get_results($sql);
    }
    
    /**
     * Adds items to user's wishlist.
     * 
     * @since 1.0
     * @access public     
     */
    public function add()
    {
        
        /**
         * Set vars.
         */           
        $raw_product    = $_REQUEST['product'];
        $raw_redirect   = $_REQUEST['redirect'];
        
        /**
         * Clean and set vars.
         */        
        $product_id     = filter_var($raw_product,FILTER_SANITIZE_NUMBER_INT);
        $redirect       = filter_var(urldecode($raw_redirect),FILTER_SANITIZE_URL);       
        
        /**
         * Run insert
         */
        $sql            = "INSERT INTO `{$this->_table}` (`user_id`,`product_id`) VALUES ('{$this->_user->id}','{$product_id}')";
        $this->_db->query($sql);
        
        /**
         * Redirect
         */
        \wp_redirect($redirect);
        exit;
    }
    
    /**
     * Remove product from user's
     * wishlist.
     * 
     * @since 1.0
     * @access public
     */
    public function delete()
    {
        /**
         * Set vars.
         */           
        $raw_product    = $_REQUEST['product'];
        $raw_redirect   = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : get_permalink();
        
        /**
         * Clean and set vars.
         */        
        $product_id     = filter_var($raw_product, FILTER_SANITIZE_NUMBER_INT);
        $redirect       = filter_var(urldecode($raw_redirect), FILTER_SANITIZE_URL);       
        
        /**
         * Run insert
         */
        $sql            = "DELETE FROM`{$this->_table}` WHERE `user_id`='{$this->_user->id}' AND `product_id`='{$product_id}'";
        $this->_db->query($sql);
        
        /**
         * Redirect
         */
        \wp_redirect($redirect);
        exit;
    }
    
    /**
     * Adds link to add product to user's
     * wishlist.
     * 
     * @since 1.0
     * @access public
     * 
     * @global (object) $post [product object]
     */
    public function add_wishlit_link()
    {
        global $post;
        
        /**
         * Link for logged in users.
         */
        $link_in = add_query_arg([
            'wishlist'=>'add',
            'product'=> $post->ID,
            'redirect'=> urlencode(get_permalink())
        ], get_permalink());
        
        /**
         * Link for logged out users.
         */
        $link_out = add_query_arg([            
            'redirect'=> urlencode($link_in)
        ], site_url(get_option('agspp_page_login')));
        
        /**
         * Show link for logged in and
         * out users.
         */
        if(is_user_logged_in() && $this->get_single($post->ID)):
            echo $this->_html->link('Already in your wishlist!',['class'=>'button wishlist-link wishlist-link-logged-in','href'=>site_url(get_option('agspp_page_user_account').'/wishlist')]);
        elseif(is_user_logged_in()):
            echo $this->_html->link('Add to Wishlist',['class'=>'button wishlist-link wishlist-link-logged-in','href'=>$link_in]);
        else:
            echo $this->_html->link('Login to Add Me to Your Wishlist',['class'=>'button wishlist-link wishlist-link-logged-out','href'=>$link_out]);
        endif;
    }
}
