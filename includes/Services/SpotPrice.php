<?php
/**
 * SpotPrice
 * 
 * All spot price related functionality.
 * 
 * Created: 02.06.17 @ 12:11 EST
 * Modified: 02.17.17 @ 11:16 EST
 * 
 * @version 1.1
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

use Agspp\Interfaces\ServiceInterface;
use Agspp\ServiceBase;

class SpotPrice extends ServiceBase implements ServiceInterface
{
    /**
     * Table name for spot prices.
     * 
     * @since 1.0
     *
     * @var string $_table
     */
    private $_table;
    
    /**
     * Version of table
     * 
     * @since 1.0
     *
     * @var float $_table_version
     */
    private $_table_version;
    
    /**
     * Name of table version option
     * in the options table.
     * 
     * @since 1.0
     *
     * @var string $_table_option
     */
    private $_table_option;

    /**
     * Class constructor.
     * 
     * Set variables and call run all
     * related WordPress actions.
     * 
     * @since 1.0
     */
    function __construct()
    {
        parent::__construct();

        /**
         * Update spot price options.
         */
        add_action('init', [$this,'wp_action_init'], 100);

        /**
         * Add admin spot price notification.
         */
        add_action('admin_notices', [$this,'wp_action_admin_notices']);
    }

    /**
     * Inject dependencies by Service Loader.
     *
     * @since 1.0
     *
     * @param array $dependencies [injected dependencies]
     */
    public function set_dependencies($dependencies) {}
    public function wp_action_wp() {}

    /**
     * Run on 'init' hook.
     *
     * Setup class variables and run
     * all necessary actions.
     *
     * @since 1.1
     */
    public function wp_action_init()
    {
        /**
         * Set table name for db.
         */
        $this->_table           = $this->_db->prefix.'spot_prices';

        /**
         * Set table version.
         */
        $this->_table_version   = 1.0;

        /**
         * Set table option name.
         */
        $this->_table_option    = 'agspp_tbl_spot_prices_vsn';

        /**
         * Set spot prices.
         */
        $this->_set_spot_price_options();
    }
    
    
    /**
     * Run on 'admin_notices' hook.
     * 
     * Adds spot prices to admin area.
     * 
     * @since 1.0
     */
    public function wp_action_admin_notices()
    {
        /**
         * Get spot prices.
         */
        $prices = $this->spot_prices_array();
        
        /**
         * Echo to screen notice div.
         */
        echo $this->_html->html_open('div', ['class'=>'notice notice-info']);
        echo $this->_html->html_open('p',['style'=>'font-weight:800']);
        echo $this->_html->html('span',['class'=>'spot-price-title'],'OUR SPOT PRICES | ');
        echo $this->_html->html('span',['class'=>'spot-price-gold'], " Gold: {$prices['gold']} |");
        echo $this->_html->html('span',['class'=>'spot-price-silver'], " Silver: {$prices['silver']} |");
        echo $this->_html->html('span',['class'=>'spot-price-platinum'], " Platinum: {$prices['platinum']} |");
        echo $this->_html->html('span',['class'=>'spot-price-palladium'], " Palladium: {$prices['palladium']}");
        echo $this->_html->html_close('p');
        echo $this->_html->html_close('div');
    }
    
    /**
     * Returns array of formatted spot prices.
     * 
     * @since 1.0
     * 
     * @return array [array of formatted spot prices]
     */
    public function spot_prices_array($color='green')
    {
        setlocale(LC_MONETARY,"en_US");
        /**
         * Array of 'money_format' spot prices.
         */
        return [
            'gold'      => money_format("<span style='font-weight:800;color:{$color}'>%i</span>", get_option('spot_price_gold')),
            'silver'    => money_format("<span style='font-weight:800;color:{$color}'>%i</span>", get_option('spot_price_silver')),
            'platinum'  => money_format("<span style='font-weight:800;color:{$color}'>%i</span>", get_option('spot_price_platinum')),
            'palladium' => money_format("<span style='font-weight:800;color:{$color}'>%i</span>", get_option('spot_price_palladium'))
        ];
    }
    
    /**
     * Returns array of un-formatted spot prices.
     * 
     * @since 1.0
     * 
     * @return array [un-formatted spot prices]
     */
    public function spot_prices_array_raw()
    {
        setlocale(LC_MONETARY,"en_US");
        /**
         * Array of 'money_format' spot prices.
         */
        return [
            'gold'      => str_replace('USD', '', money_format("%i", get_option('spot_price_gold'))),
            'silver'    => str_replace('USD', '', money_format("%i", get_option('spot_price_silver'))),
            'platinum'  => str_replace('USD', '', money_format("%i", get_option('spot_price_platinum'))),
            'palladium' => str_replace('USD', '', money_format("%i", get_option('spot_price_palladium')))
        ];
    }

    /**
     * set_spot_price_options
     *
     * Update spot price options to current
     * spot prices.
     *
     * @since 1.0
     */
    private function _set_spot_price_options()
    {
        $prices = $this->_db->get_results("SELECT * FROM {$this->_table}");
        foreach($prices as $price):
            update_option('spot_price_'.$price->metal,$price->ask);
        endforeach;
    }
}