<?php
/**
 * Agspp
 * 
 * Plugin Admin class.
 * 
 * Created: 02.09.17 @ 09:03 EST
 * Modified: 02.21.17 @ 22:42 EST
 * 
 * @version 1.0
 * 
 * @package    JWieczorek
 * @subpackage agspp
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

use Agspp\Interfaces\ServiceInterface;
use Agspp\ServiceBase;

class Agspp_Admin extends ServiceBase implements ServiceInterface
{
    /**
     * Admin constructor.
     */
    function __construct() 
    {
        parent::__construct();

        /**
         * Run on init.
         */
        add_action('admin_init', [$this, 'wp_action_wp']);

        /**
         * Run on admin menu
         */
        add_action('admin_menu', [$this, 'run_on_admin_menu']);

        /**
         * Show extra user profile fields
         */
        add_action('show_user_profile', [$this, 'extra_profile_fields']);
        add_action('edit_user_profile', [$this, 'extra_profile_fields']);
    }

    function set_dependencies($dependencies) {}

    /**
     * run_admin_on_init
     *
     * Runs on 'admin_init' hook.
     *
     * @since 1.0
     * @access public
     */
    function wp_action_wp()
    {
        if(isset($_POST['agspp-admin-form'])):
            $this->_save_options();
        endif;
    }
    
    /**
     * run_on_admin_menu
     * 
     * Runs on 'admin_menu' hook.
     * 
     * @since 1.0
     * @access public
     */
    public function run_on_admin_menu()
    {
        add_menu_page('AGSPP Settings', 'AGSPP', 'administrator', 'agspp', [$this, 'add_admin_page'], '');
    }
    
    /**
     * add_admin_page
     * 
     * Adds content to admin page.
     * 
     * @since 1.0
     * @access public
     */
    public function add_admin_page()
    {
        include AGSPP_PATH . 'assets/html/admin.settings.html.php';
    }
    
    /**
     * extra_profile_fields
     * 
     * Shows extra profile fields on the admin
     * user profile page.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (object) $user [WP_User object]
     */
    public function extra_profile_fields($user)
    {
        include AGSPP_PATH . 'assets/html/admin.profile-fields.html.php';
    }
    
    /**
     * _save_options
     * 
     * Saves admin options.
     * 
     * @since 1.0
     * @access private
     */
    private function _save_options()
    {
        /**
         * Loop through options clean
         * and save.
         */
        foreach($_POST['agspp_options'] as $name => $option_raw):
            if($name==='agspp_before_product_info_text'):                
                update_option($name, stripcslashes($option_raw));
            else:
                $option = filter_var($option_raw, FILTER_SANITIZE_STRING);
                update_option($name, stripcslashes($option));
            endif;                        
        endforeach;
    }
}