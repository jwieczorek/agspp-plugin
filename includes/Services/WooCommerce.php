<?php
/**
 * WooCommerce
 * 
 * Customizes Woocommerce.
 * 
 * Created: 02.05.17 @ 13:34 EST
 * Modified: 02.17.17 @ 07:49 EST
 * 
 * @version 1.1
 * 
 * @package    JWieczorek
 * @subpackage agspp
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

use Agspp\Interfaces\ServiceInterface;
use Agspp\ServiceBase;

class WooCommerce extends ServiceBase implements ServiceInterface
{
    /**
     * Plugin's portfolio class.
     * 
     * @since 1.0
     * @access private
     * 
     * @var (object) [Portfolio class] 
     */
    private $_portfolio;
    
    /**
     * Plugin's wishlist class.
     * 
     * @since 1.0
     * @access private
     * 
     * @var (object) [Wishlist class] 
     */
    private $_wishlist;

    /**
     * Bulk pricing meta key prefix.
     *
     * @since 1.0
     * @access private
     *
     * @var $_bk_pricing_key
     */
    private $_bk_pricing_key_prfx;

    /**
     * Bulk pricing post field name.
     *
     * @since 1.0
     * @access private
     *
     * @var $_bk_pricing_field
     */
    private $_bk_pricing_field;

    /**
     * Class constructor.
     * 
     * Set portfolio object and run
     * class.
     * 
     * @since 1.0
     */
    function __construct()
    {
        parent::__construct();
        /**
         * Set bulk pricing meta prefix.
         */
        $this->_bk_pricing_key_prfx = '_bk_t1_q';

        /**
         * Set bulk pricing post name.
         */
        $this->_bk_pricing_field    = '_bulk_pricing[%s]';

        /**
         * Run woocommerce class.
         */
        $this->run();
    }

    public function set_dependencies($dependencies)
    {
        $this->_portfolio   = $dependencies['portfolio'];
        $this->_wishlist    = $dependencies['wishlist'];
    }


    public function wp_action_wp()
    {
        global $post;

        /**
         * Remove price of metal.
         */
        if(!is_admin() && 'metal'===get_post_meta($post->ID, '_product_type',1)):
            remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        endif;
    }

    /**
     * run
     * 
     * Run actions and filters.
     * 
     * @since 1.0
     * @access public
     */
    public function run()
    {       
        /**
         * Add end point query args.
         */
        add_filter('query_vars', [$this,'add_query_args'], 100);
        
        /**
         * Add 'metals-portfolio' endpoint to WP.
         */
        add_action('init', [$this,'woo_add_endpoints'], 100);
        
        /**
         * Add my 'metals-portfolio' endpoint.
         */
        add_action('woocommerce_account_metals-portfolio_endpoint', [$this,'woo_tab_metals_portfolio_content']);
        
        /**
         * Add my 'wishlist' endpoint.
         */
        add_action('woocommerce_account_wishlist_endpoint', [$this,'woo_tab_wishlist_content']);
        
        /**
         * Add my 'wishlist' endpoint.
         */
        add_action('woocommerce_account_contact-us_endpoint', [$this,'woo_tab_contact_us_content']);

        /**
         * Add my 'order-tracking' endpoint.
         */
        add_action('woocommerce_account_order-tracking_endpoint', [$this,'woo_tab_order_tracking_content']);
        
        /**
         * Add menu items to user account page.
         */
        add_filter('woocommerce_account_menu_items', [$this,'woo_account_add_links']);
        
        /**
         * Add metals user's to portfolio.
         */
        add_action('woocommerce_order_status_completed', [$this,'woo_add_portfolio_metals']);

        /**
         * Email customer shipping information.
         */
        add_action('woocommerce_order_status_shipped', [$this,'woo_shipped_tracking_email']);
        
        /**
         * Add new user when manual order is 
         * processed.
         */
        add_action('woocommerce_process_shop_order_meta', [$this,'woo_add_manual_user'], 9999, 2);
        
        /**
         * Rename 'Additional Information' tab.
         */
        add_filter('woocommerce_product_tabs', [$this,'woo_rename_product_tabs'], 98);
        
        /**
         * Add bulk pricing tab to Product Data 
         * metabox.
         */
        add_filter('woocommerce_product_data_tabs', [$this,'woo_add_product_metabox_tabs']);
        
        /**
         * Add fields to bulk pricing tab.
         */
        add_action('woocommerce_product_data_panels', [$this,'woo_bulk_pricing_tab_content']);
        
        /**
         * Save product meta.
         */
        add_action('woocommerce_process_product_meta', [$this,'woo_save_product_meta'], 99999);       
        
        /**
         * Add weight and type to general panel.
         */
        add_action('woocommerce_product_options_general_product_data', [$this,'woo_add_data_to_general_fields']);
        
        /**
         * Add notice above product info.
         */
        add_action('woocommerce_single_product_summary', [$this,'woo_add_data_after_price'], 19);       
        
        /**
         * Create prices based on spot price and quantity.
         */
        add_filter('woocommerce_get_price', [$this,'woo_get_price'], 9999, 2);     
        add_filter('woocommerce_cart_item_price', [$this,'woo_cart_item_price'], 10, 3);
        
        /**
         * Check whether to charge tax.
         */        
        add_action('woocommerce_before_calculate_totals', [$this,'woo_before_calculate_totals'], 9998);
        
        /**
         * Remove tax from items.
         */
        add_action('woocommerce_calculate_totals', [$this,'woo_calculate_totals'], 9999);        
        
        /**
         * Plugin custom Woocommerce templates 
         * location.
         */
        add_filter('woocommerce_locate_template', [$this, 'woo_template_location'], 999, 3);

        /**
         * Create shipped order status.
         */
        //add_action('init', [$this, 'woo_create_order_status_shipped']);

        /**
         * Add shipped status to order manager.
         */
        //add_filter('wc_order_statuses', [$this, 'woo_add_order_status_shipped']);

        /**
         * Add shipping info metabox.
         */
        //add_action('add_meta_boxes', [$this, 'woo_add_shipping_options_metabox']);

        /**
         * Save product shipping info.
         */
        //add_action('save_post', [$this, 'woo_save_traking_info']);

        add_filter('woocommerce_available_payment_gateways', [$this, 'woo_delete_payment_gateways']);

    }
    
    /**
     * add_query_args
     * 
     * Adds query arguments to WordPress.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (array) $args
     * 
     * @return (array) [query arguments to add]
     */
    public function add_query_args($args)
    {
        /**
         * Add 'metals portfolio' to query args.
         */
        $args[] = 'metals-portfolio';
        
        /**
         * Add 'wishlist' to query args.
         */
        $args[] = 'wishlist';
        
        /**
         * Add 'contact-us' to query args.
         */
        $args[] = 'contact-us';

        /**
         * Add 'order-tracking' to query args.
         */
        $args[] = 'order-tracking';
        
        return $args;
    }
    
    /**
     * woo_add_metals_portfolio_endpoint
     * 
     * Add 'metals-portfolio' rewrite endpoint to WordPress.
     * 
     * @since 1.0
     * @access public
     */
    public function woo_add_endpoints()
    {
        add_rewrite_endpoint('metals-portfolio', EP_ROOT | EP_PAGES);
        add_rewrite_endpoint('wishlist', EP_ROOT | EP_PAGES);
        add_rewrite_endpoint('contact-us', EP_ROOT | EP_PAGES);
        add_rewrite_endpoint('order-tracking', EP_ROOT | EP_PAGES);
    }
    
    /**
     * woo_tab_metals_portfolio_content
     * 
     * Content for 'metals-portfolio' endpoint.
     * 
     * @since 1.0
     * @access public
     */
    public function woo_tab_metals_portfolio_content()
    {        
        echo '<div id="user-metals-portfolio-chart-container" data-uid="'.get_current_user_id().'" data-url="'.admin_url( 'admin-ajax.php' ) .'"></div>';
    }
    
    /**
     * woo_tab_wishlist_content
     * 
     * Content for wishlist endpoint.
     * 
     * @since 1.0
     * @access public
     */
    public function woo_tab_wishlist_content()
    {        
        $wishlist   = $this->_wishlist->get();        
        include AGSPP_TEMPLATES_PATH . 'agspp/account-wishlist.html.php';
    }
    
    /**
     * woo_tab_contact_us_content
     * 
     * Content for contact-us endpoint.
     * 
     * @since 1.0
     * @access public
     */
    public function woo_tab_contact_us_content()
    {
        $phone      = get_option('agspp_phone_number');
        $form_id    = get_option('agspp_customer_contact_form');        
        $shortcode  = sprintf('[contact-form-7 id="%s"]',$form_id);        
        echo '<h2 class="kad_endpointtitle">Contact Us</h2>';        
        echo "<p>You may call us at <strong>{$phone}</strong> or fill out the form below!</p>";        
        echo do_shortcode($shortcode);
    }

    /**
     * woo_tab_order_tracking_content
     *
     * Content for order-tracking endpoint.
     *
     * @since 1.0
     * @access public
     */
    public function woo_tab_order_tracking_content()
    {
        echo 'Tracking...';
    }
    
    /**
     * woo_account_add_links
     * 
     * Add links to user account page.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (array) $items [account page links]
     * 
     * @return (array) [menu items for account page]
     */
    public function woo_account_add_links($items)
    {
        return [
            'dashboard'     => 'Dashboard',
            'edit-account'     => 'Account',
            'edit-address'     => 'Addresses',
            'orders'        => 'Orders',
//            'order-tracking'=> 'Tracking',
            'metals-portfolio'=> 'Portfolio',
            'wishlist'      => 'Wishlist',
            'contact-us'    => 'Contact Us',
            'logout'=>'Logout'
        ];
    }
    
    /**
     * woo_add_portfolio_metals
     * 
     * When order is completed add metals to user's 
     * portfolio.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (int) $order_id [id of order]
     */
    public function woo_add_portfolio_metals($order_id)
    {
        /**
         * Get order and user id to 
         * pass to portfolio object.
         */
        $order      = wc_get_order($order_id);
        $user_id    = $order->user_id ? (int) $order->user_id : 0;
        
        /**
         * If no user then stop processing.
         */
        if(!$user_id):
            return;
        endif;
                
        /**
         * Add metals to portfolio.
         */
        $this->_portfolio->add_to_portfolio($order,$order_id,$user_id);
    }
    
    /**
     * woo_add_manual_user
     * 
     * When manual order is processed add 
     * user to WordPress users.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (int) $order_id [order id]
     */
    public function woo_add_manual_user($order_id=0)
    {
        /**
         * If user exists then exit.
         */
        if($this->_user->get_by('email','billing_email')):
            return;
        endif;
        
        /**
         * Generate random password.
         */
        $password= agspp_random_password();
        
        /**
         * Insert new user
         */
        $AGSPP_USER->add('billing_email', 'billing_first_name', 'billing_last_name', $password);       
        
        /**
         * If user was successfully inserted then
         * add customer to order and add user
         * meta and send email.
         */
        if($this->_user->id > 0):
            update_post_meta($order_id, '_customer_user', $this->_user->id);
            $this->_user->update_meta(agspp_manual_user_meta(), '', $this->_user->id);
            /**
             * Setup email data and send email.
             */
            $data = [
                'user_login'    => _field('billing_email'),
                'blogname'      => get_option('blogname'),
                'email_heading' => 'Welcome to American Gold | SPP',
                'password'      => $password
            ];

            _email(_field('billing_email'), 'Your account on American Gold | SPP', $data, 'emails/customer-new-account.php');
        endif;    
        
    }
    
    /**
     * woo_rename_product_tabs
     * 
     * Rename product tabs.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (array) $tabs [product tabs array]
     */    
    public function woo_rename_product_tabs($tabs)
    {
        /**
         * Change 'Additional Information' tab 
         * text.
         */
        $tabs['additional_information']['title'] = __( 'Product Details' );
        
        return $tabs;
    }
    
    /**
     * woo_add_product_metabox_tabs
     * 
     * Add tabs to Product metabox.
     * 
     * @since 1.0
     * @access public
     * 
     * @param type $product_data_tabs [product data tabs]
     */
    public function woo_add_product_metabox_tabs($product_data_tabs)
    {
        /**
         * Add bulk pricing tab.
         */
        $product_data_tabs['bulk-pricing'] = array(
            'label' => __( 'Bulk Pricing' ),
            'target' => 'bulk_pricing_product_data',
	);        
        
	return $product_data_tabs;
    }
    
    /**
     * woo_add_data_to_general_fields
     * 
     * Add fields to product data box 
     * in the general tab.
     * 
     * @since 1.0
     * @access public
     */
    public function woo_add_data_to_general_fields()
    {
        woocommerce_wp_text_input([ 
            'id'            => '_apmex_product_id',            
            'label'         => 'APMEX Id',
            'description'   => 'APMEX Product Id!',
            'default'       => '0',
            'desc_tip'      => true,
            'placeholder'   => 'APMEX Product Id!'
        ]);
        
        woocommerce_wp_select([
            'id'            => '_product_type',
            'label'         => 'Product Type',
            'options'       => [
                ''      => '',
                'metal' => 'Metal',
                'other' => 'Other'
            ]
        ]);
        
        woocommerce_wp_select([
            'id'            => '_product_metal',
            'label'         => 'Product Metal',
            'options'       => [
                ''      => '',
                'gold'  => 'Gold',
                'silver' => 'Silver',
                'platinum'=> 'Platinum',
                'palladium'=> 'Palladium'
            ]
        ]);
        
        woocommerce_wp_text_input([ 
            'id'            => '_product_weight',            
            'label'         => 'Weight',
            'description'   => 'Weight in troy ounces!',
            'default'       => '0',
            'desc_tip'      => true,
            'placeholder'   => 'Weight in troy ounces!'
        ]);
        
        echo Form::html('p',[],'<a href="https://jscalc.io/calc/NzI8WHryU3tHWka9" target="_blank">Weight Calculator</a>');
    }
    
    /**
     * woo_bulk_pricing_tab_content
     * 
     * Add fields to product data box 
     * in the bulk pricing tab.
     * 
     * @since 1.0
     * @access public
     */
    public function woo_bulk_pricing_tab_content()
    {
        echo '<div id="bulk_pricing_product_data" class="panel woocommerce_options_panel">';               
        echo '<p style="font-weight: bold"><a href="https://jscalc.io/calc/aEjUJSRWw89t79N1" target="_blank">Price Calculator</a><br>Tier 1</p>';
        woocommerce_wp_text_input([ 
            'id'            => '_bk_t1_q1', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Start Qty',
            'description'   => 'Starting quantity (e.g. "1" in 1-10)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'Start Qty!'
        ]);
        woocommerce_wp_text_input([
            'id'            => '_bk_t1_q2', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Start Qty',
            'description'   => 'End quantity (e.g. "10" in 1-10)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'End Qty!'
        ]);
        woocommerce_wp_text_input([
            'id'            => '_bk_t1_price', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Price',
            'description'   => 'Price (product price)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'Price!'
        ]);
        echo '<p style="font-weight: bold">Tier 2</p>';
        woocommerce_wp_text_input([
            'id'            => '_bk_t2_q1', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Start Qty',
            'description'   => 'Starting quantity (e.g. "11" in 11-20)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'Start Qty!'
        ]);
        woocommerce_wp_text_input([ 
            'id'            => '_bk_t2_q2', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Start Qty',
            'description'   => 'End quantity (e.g. "20" in 11-20)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'End Qty!'
        ]);
        woocommerce_wp_text_input([ 
            'id'            => '_bk_t2_price', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Price',
            'description'   => 'Price (product price)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'Price!'
        ]);
        echo '<p style="font-weight: bold">Tier 3</p>';
        woocommerce_wp_text_input([
            'id'            => '_bk_t3_q1', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Start Qty',
            'description'   => 'Starting quantity (e.g. "21" in 21-100)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'Start Qty!'
        ]);
        woocommerce_wp_text_input([
            'id'            => '_bk_t3_q2', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Start Qty',
            'description'   => 'End quantity (e.g. "100" in 21-100)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
                'placeholder' => 'End Qty!'
        ]);                
        woocommerce_wp_text_input([
            'id'            => '_bk_t3_price', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Price',
            'description'   => 'Price (product price)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'Price!'
        ]);
        echo '<p style="font-weight: bold">Tier 4</p>';
        woocommerce_wp_text_input([
            'id'            => '_bk_t4_q1', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Start Qty',
            'description'   => 'Starting quantity (e.g. "101" in 101-500)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'Start Qty!'
        ]);
        woocommerce_wp_text_input([
            'id'            => '_bk_t4_q2', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Start Qty',
            'description'   => 'End quantity (e.g. "500" in 101-500)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
                'placeholder' => 'End Qty!'
        ]);                
        woocommerce_wp_text_input([
            'id'            => '_bk_t4_price', 
            'type'  => 'text',
            'wrapper_class' => 'show_if_simple', 
            'label'         => 'Price',
            'description'   => 'Price (product price)',
            'default'  		=> '0',
            'desc_tip'    	=> true,
            'placeholder' => 'Price!'
        ]);
	echo '</div>';
    }
    
    /**
     * woo_save_bulk_pricing
     * 
     * Saves bulk prices.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (int) $product_id [id of product]
     */
    public function woo_save_product_meta($product_id)
    {
        /**
         * Save product type, metal and weight
         * SKU number, and APMEX product id.
         */
        update_post_meta($product_id, '_apmex_product_id', filter_input(INPUT_POST, '_apmex_product_id' , FILTER_SANITIZE_STRING));
        update_post_meta($product_id, '_product_type', filter_input(INPUT_POST, '_product_type' , FILTER_SANITIZE_STRING));
        update_post_meta($product_id, '_product_metal', filter_input(INPUT_POST, '_product_metal' , FILTER_SANITIZE_STRING));
        update_post_meta($product_id, '_product_weight', filter_input(INPUT_POST, '_product_weight' , FILTER_SANITIZE_STRING));
        update_post_meta($product_id, '_weight', filter_input(INPUT_POST, '_product_weight' , FILTER_SANITIZE_STRING));
        update_post_meta($product_id, '_sku', filter_input(INPUT_POST, 'post_ID' , FILTER_SANITIZE_STRING));
        /**
         * Cleaned prices and limits.
         */
        $prices = $this->_woo_clean_bulk_pricing();
        
        /**
         * Save values.
         */
        $this->_woo_update_save_bulk_pricing($product_id, $prices);
    }
    
    /**
     * woo_custom_product_price
     * 
     * Generate product price based on spot
     * price and metal weight.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $price [original product price]
     * @param (object) $product [product object]
     * 
     * @return (float) [calculated product price]
     */
    public function woo_get_price($price,$product)
    {
        /**
         * If not metal return price.
         */
        if('metal'!==get_post_meta($product->id, '_product_type', 1)) :
           return $price; 
        endif;
        
        /**
         * Return calculated price.
         */
        return agspp_get_metal_item_price($product->id,1, $price); 
    }
    
    /**
     * woo_cart_item_price
     * 
     * Edit item prices by quantity and bulk pricing.
     * 
     * @param (object) $cart [woocommerce cart object]
     */
    public function woo_cart_item_price($price,$item)
    {
        if('metal'===get_post_meta($item['product_id'], '_product_type', 1)):            
            $price = agspp_get_metal_item_price($item['product_id'], $item['quantity'], $price);
        endif;
        
        return $price;       
    }
    
    /**
     * woo_before_calculate_totals
     * 
     * See if tax needs to be charged.
     * 
     * @param (object) $cart [woocommerce cart object]
     */
    public function woo_before_calculate_totals($cart)
    {
        $cart_totals = [];
        
        foreach($cart->cart_contents as $item):
            if('metal'===get_post_meta($item['product_id'], '_product_type', 1)):               
                array_push($cart_totals, $item['line_total']);                
            endif;
        endforeach;            
        
        if(array_sum($cart_totals) >= 500):
            $this->_session->set('_agspp_remove_tax', true);
            return;
        endif;
        
        $this->_session->set('_agspp_remove_tax', false);
    }
    
    /**
     * woo_remove_item_tax
     * 
     * Remove tax from item.
     * 
     * @since 1.0
     * @access private
     * 
     * @param (object) $cart [woocommerce cart object]
     */
    public function woo_calculate_totals($cart)
    {
        global $woocommerce;

        /**
         * Add paypal/cc fee.
         */
        $this->_woo_add_paypal_cc_fee($cart);

        /**
         * If metals total is not at least 
         * $500 return and quit.
         */
        if(false===$this->_session->get('_agspp_remove_tax')):
            return;
        endif;
        
        /**
         * Loop through cart contents and find 
         * metals.
         */
        foreach($cart->cart_contents as $key => $item):
            /**
             * If it is a metal, remove tax and remove
             * tax from cart.
             */            
            if('metal'===get_post_meta($item['product_id'], '_product_type', 1)):               
                $cart->taxes[1] -= $item['line_tax'];
                $cart->tax_total -= $item['line_tax'];
                $item['line_tax'] = 0;
                $item['line_subtotal_tax'] = 0;
                $item['line_tax_data'] = [];
                $cart->cart_contents[$key] = $item;
            endif; 
            
        endforeach;
        
        
    }
    
    /**
     * woo_add_product_notice
     * 
     * Adds notice above product's info.
     * 
     * @since 1.0
     * @access public
     */
    public function woo_add_data_after_price()
    {
        global $post;        
        
        echo get_option('agspp_before_product_info_text') ? '<div class="agspp-product-notice">'.get_option('agspp_before_product_info_text').'</div>' : ''; 
        
        /**
         * Bulk pricing table.
         */
        $weight     = get_post_meta($post->ID, '_product_weight', 1);
        $metal      = get_post_meta($post->ID, '_product_metal', 1);
        $tiers      = get_post_meta($post->ID, '_bulk_pricing', 1);
        $SpotPrice  = get_option('spot_price_'.$metal);
        $cc_pp_cg   = get_option('agspp_cc_paypal_charge');        
        $spot_price = ($SpotPrice*$weight);
        include AGSPP_TEMPLATES_PATH . 'agspp/product.bulk-pricing.html.php';
    }
    
    /**
     * woo_template_location
     * 
     * Custom Woocommerce template file
     * location.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $template [default template]
     * @param (string) $template_name [default template name]
     * @param (string) $template_path [default template path]
     * 
     * @return (string) [custom template]
     */
    public function woo_template_location($template, $template_name, $template_path)
    {
        global $woocommerce;
        
        $_template = $template;
 
        /**
         * Verify template path is 
         * valid and set.
         */
        if (!$template_path):
            $template_path = $woocommerce->template_url;
        endif;
        
        /**
         * Plugin Woocommerce template
         * location.
         */
        $plugin_path  = AGSPP_TEMPLATES_PATH.'woocommerce/templates/';
        
        /**
         * See if template file exists 
         * in current theme.
         */
        $theme_template = locate_template([
              $template_path . $template_name,
              $template_name
        ]);       
        
        /**
         * If template is not in 
         * them, check if exists
         * in the plugin and 
         * return it if so.
         */
        if (''==$theme_template && file_exists($plugin_path.$template_name)):
            $template = $plugin_path.$template_name;
        endif;        
        
        /**
         * If no template, then
         * return default.
         */
        if (''==$template):
            $template = $_template;
        endif;
        
        return $template;         
    }

    /**
     * woo_create_order_status_shipped
     *
     * Create custom order status.
     *
     * @since 1.0
     * @access public
     */
    public function woo_create_order_status_shipped()
    {
        register_post_status( 'wc-shipped', array(
            'label'                     => 'Shipped',
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop( 'Shipped <span class="count">(%s)</span>', 'Shipped <span class="count">(%s)</span>' )
        ) );
    }

    /**
     * woo_add_order_status_shipped
     *
     * Add order status to order manager.
     *
     * @since 1.0
     * @access public
     *
     * @param (array) $order_statuses [array of order statuses]
     * @return (array) [order statuses]
     */
    public function woo_add_order_status_shipped($order_statuses)
    {
        $order_statuses['wc-shipped'] = _x('Shipped', 'Order status', 'woocommerce');
        return $order_statuses;
    }

    /**
     * woo_shipped_tracking_email
     *
     * Email customer when item is shipped.
     *
     * @since 1.0
     * @access public
     *
     * @param (int) $order_id [id of order]
     */
    public function woo_shipped_tracking_email($order_id)
    {
        wp_mail('joshua.d.wieczorek@gmail.com','Shipped','Your order has shipped.');
    }

    /**
     * woo_add_shipping_options_metabox
     *
     * Add metabox to admin order panel.
     *
     * @since 1.0
     * @access public
     */
    public function woo_add_shipping_options_metabox()
    {
        add_meta_box( 'agspp_shipping_info', __('Shipping Info','woocommerce'), [$this, 'woo_add_shipping_options_metabox_content'], 'shop_order', 'side', 'core' );
    }

    /**
     *
     */
    public function woo_add_shipping_options_metabox_content()
    {
        global $post;

        $shipping_carrier = get_post_meta($post->ID, '_order_tracking_carrier', 1);
        $tracking_number = get_post_meta($post->ID, '_order_tracking_number', 1);

        echo Form::html_open('p');
        echo Form::html('label', ['for' => '_order_tracking_carrier'], 'Carrier: ');
        echo '<select name="_order_tracking_carrier" id="_order_tracking_carrier">';
        echo '<option>Select...</option>';
        echo '<option value="usps" ',($shipping_carrier=='usps'? 'selected="selected"' : ''),'>USPS</option>';
        echo '<option value="ups"',($shipping_carrier=='ups'? 'selected="selected"' : ''),'>UPS</option>';
        echo '<option value="fedex" ',($shipping_carrier=='fedex'? 'selected="selected"' : ''),'>FedEx</option>';
        echo '<option value="dhl" ',($shipping_carrier=='dhl'? 'selected="selected"' : ''),'>DHL</option>';
        echo '</select>';
        echo Form::html_close('p');

        echo Form::html_open('p');
        echo Form::html('label', ['for' => '_order_tracking_number'], 'Traking #: ');
        echo Form::input(['type' => 'text', 'name' => '_order_tracking_number', 'id' => '_order_tracking_number', 'value' => $tracking_number]);
        echo Form::html_close('p');
    }

    /**
     * woo_save_traking_info
     *
     * Save tracking information.
     *
     * @since 1.0
     * @access public
     *
     * @param (int) $post_id [id of saved post]
     */
    public function woo_save_traking_info($post_id)
    {
        $post_type = get_post_type($post_id);

        if('shop_order'!== $post_type):
            return;
        endif;

        update_post_meta($post_id, '_order_tracking_carrier', filter_input(INPUT_POST, '_order_tracking_carrier' , FILTER_SANITIZE_STRING));
        update_post_meta($post_id, '_order_tracking_number', filter_input(INPUT_POST, '_order_tracking_number' , FILTER_SANITIZE_STRING));
    }

    /**
     * woo_delete_payment_gateways
     *
     * Removes payment gateways based on cart total.
     *
     * @since 1.0
     * @access public
     *
     * @param array $gateways
     *
     * @return array [array of usable payment gateways]
     */
    public function woo_delete_payment_gateways($gateways=[])
    {
        global $woocommerce;

        $cart_total =  (float) preg_replace('/[^1234567890\.]/', '', $woocommerce->cart->total);

        if($cart_total >= 500000.00):
            unset($gateways['firstdata']);
            unset($gateways['paypal']);
            unset($gateways['cheque']);
        elseif ($cart_total >= 250000.00):
            unset($gateways['paypal']);
            unset($gateways['cheque']);
        elseif ($cart_total >= 5000.00):
            unset($gateways['paypal']);
        endif;

        return $gateways;
    }
    
    /** 
     * _woo_clean_bulk_pricing
     * 
     * Cleans and sanitizes bulk pricing
     * fields.
     * 
     * @since 1.0
     * @access private
     * 
     * @return (array) 
     */
    private function _woo_clean_bulk_pricing()
    {
        return [
            't1' => [
                '_bk_t1_q1' => filter_input(INPUT_POST, '_bk_t1_q1', FILTER_SANITIZE_NUMBER_INT),
                '_bk_t1_q2' => filter_input(INPUT_POST, '_bk_t1_q2', FILTER_SANITIZE_NUMBER_INT),
                '_bk_t1_price' => filter_input(INPUT_POST, '_bk_t1_price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
            ],
            't2' => [
                '_bk_t2_q1' => filter_input(INPUT_POST, '_bk_t2_q1', FILTER_SANITIZE_NUMBER_INT),
                '_bk_t2_q2' => filter_input(INPUT_POST, '_bk_t2_q2', FILTER_SANITIZE_NUMBER_INT),
                '_bk_t2_price' => filter_input(INPUT_POST, '_bk_t2_price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
            ],
            't3' => [
                '_bk_t3_q1' => filter_input(INPUT_POST, '_bk_t3_q1', FILTER_SANITIZE_NUMBER_INT),
                '_bk_t3_q2' => filter_input(INPUT_POST, '_bk_t3_q2', FILTER_SANITIZE_NUMBER_INT),
                '_bk_t3_price' => filter_input(INPUT_POST, '_bk_t3_price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
            ],
            't4' => [
                '_bk_t4_q1' => filter_input(INPUT_POST, '_bk_t4_q1', FILTER_SANITIZE_NUMBER_INT),
                '_bk_t4_q2' => filter_input(INPUT_POST, '_bk_t4_q2', FILTER_SANITIZE_NUMBER_INT),
                '_bk_t4_price' => filter_input(INPUT_POST, '_bk_t4_price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
            ]           
        ];
    }
    
    /**
     * _woo_update_save_bulk_pricing
     * 
     * Saves bulk pricing meta fields and single
     * meta value.
     * 
     * @since 1.0
     * @access private
     * 
     * @param (int) $product_id [product id]
     * @param (array) $prices [cleaned price values]
     */
    private function _woo_update_save_bulk_pricing($product_id,$prices)
    {
        $bulk_prices = [];
        
        foreach($prices as $tier => $data):
            /**
             * Extract value from array and 
             * create new array.
             */
            $bulk_prices[$tier]['from'] = implode('',array_values(array_slice($data,0,1)));
            $bulk_prices[$tier]['to'] = implode('',array_values(array_slice($data,1,1)));
            $bulk_prices[$tier]['price'] = implode('',array_values(array_slice($data,2,1)));
            /**
             * Update individual values.
             */
            $this->_woo_update_save_bulk_pricing_meta_vals($product_id, $data);
        endforeach;
        
        /**
         * Update bulk pricing meta.
         */
        update_post_meta($product_id,'_bulk_pricing',$bulk_prices);
    }
    
    /**
     * _woo_update_save_bulk_pricing_meta_vals
     * 
     * Saves bulk pricing meta fields and single
     * meta value.
     * 
     * @since 1.0
     * @access private
     * 
     * @param (int) $product_id [product id]
     * @param (array) $prices [cleaned price values]
     */
    private function _woo_update_save_bulk_pricing_meta_vals($product_id,$prices)
    {
        foreach($prices as $key => $value):
            update_post_meta($product_id,$key,$value);
        endforeach;
    }
    
    /**
     * _woo_add_paypal_cc_fee
     * 
     * Adds paypal/cc fee to payment page.
     * 
     * @since 1.0
     * @access private    
     * 
     * @param (objest) $cart [cart object]
     */
    private function _woo_add_paypal_cc_fee($cart)
    {
        global $woocommerce;
        
        /**
         * Set paypal/cc fee, calculate fee total
         * and set gateway session.
         */
        $paypal_cc_fee      = (float) get_option('agspp_cc_paypal_charge');
        $fee_total          = (float) ($cart->cart_contents_total*$paypal_cc_fee);        
        $gateway_session    = isset($woocommerce->session->chosen_payment_method) ? $woocommerce->session->chosen_payment_method : null;

        /**
         * If selected gateway is paypal or firstdata
         * then add the fee.
         */
        if (null !== $gateway_session && $gateway_session=='paypal'): 
            $cart->add_fee('PayPal Processing Fee', $fee_total);
            $cart->cart_contents_total = ($cart->cart_contents_total+$fee_total);
        elseif(null !== $gateway_session && $gateway_session=='firstdata'):
            $cart->add_fee('Credit Card Processing Fee', $fee_total);
            $cart->cart_contents_total = ($cart->cart_contents_total+$fee_total);
        endif;      
    }
}