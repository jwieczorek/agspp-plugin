<?php
/**
 * Session
 *
 * PHP Session wrapper.
 *
 * Created: 02.06.17 @ 12:11 EST
 * Modified: 02.17.17 @ 11:16 EST
 *
 * @version 1.1
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 */

namespace Agspp\Services;


class Sessions
{
    /**
     * Session constructor.
     *
     * Start session if no sessions
     * are started.
     */
    public function __construct()
    {
        if(!session_id()):
            session_start();
        endif;
    }

    /**
     * Sets a session key/value. If it
     * does not exist.
     *
     * @since 1.0
     *
     * @param string $key [name of key to set]
     * @param string $value [value of session key]
     */
    public function set($key='', $value='')
    {
        if(''!==$key)
            $_SESSION[$key] = $value;
    }

    /**
     * Gets a session value from
     * a key.
     *
     * @since 1.0
     *
     * @param string $key [name of key/value to get]
     *
     * @return string [value of session key]
     */
    public function get($key='')
    {
        if(''!==$key)
            return isset($_SESSION[$key]) ? $_SESSION[$key] : '';
    }

    /**
     * Gets a key and unsets it
     * afterwards.
     *
     * @param string $key [name of key to get]
     *
     * @return string
     */
    public function flash($key='')
    {
        $variable = '';

        if(''!==$key && isset($_SESSION[$key])):
            $variable = $_SESSION[$key];
            unset($_SESSION[$key]);
        endif;

        return $variable;
    }

    /**
     * Gets all session variables.
     *
     * @since 1.0
     *
     * @return array [all session variables]
     */
    public function all()
    {
        return $_SESSION;
    }

    /**
     * Unsets a key form the session.
     *
     * @since 1.0
     *
     * @param string $key [name of key to remove]
     */
    public function delete($key='')
    {
        if(''!==$key && isset($_SESSION[$key])):
             unset($_SESSION[$key]);
        endif;

    }

    /**
     * Checks to see if a session variable exists.
     *
     * @param string $key [name of session to check]
     * @return bool [true/false if session key exists]
     */
    public function has($key='')
    {
        return isset($_SESSION[$key]) ? true : false;
    }
}