<?php
/**
 * Html
 * 
 * Creates html elements.
 * 
 * Created: 02.03.17 @ 18:13 EST
 * Modified: 02.20.17 @ 10:34 EST
 *
 *
 * @version 1.0
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

class Html
{
    /**
     * html_open
     * 
     * Creates and returns opening html tag.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $tag [desired html tag]
     * @param (array) $attributes [e.g. class, id, type, name]
     * 
     * @return (string) [opening html tag with attributes]
     */
    public function html_open($tag='p',$attributes=[])
    {
        return "<{$tag}{$this->_attributes($attributes)}>";
    }   
    
    /**
     * html_close
     * 
     * Creates and returns closing html tag.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $tag [desired html tag]
     * @return (string) [closing html tag]
     */
    public function html_close($tag='p')
    {
        return "</{$tag}>";
    }
    
    /**
     * html
     * 
     * Creates html element and places content
     * inside.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $tag [html tag to create]
     * @param (array) $attributes [e.g. class, id, type, name]
     * @param (string) $content [content to place inside tag]
     * 
     * @return (string) [html tag and content]
     */
    public function html($tag='p',$attributes=[],$content='')
    {        
        $html  = "<{$tag}{$this->_attributes($attributes)}>";
        $html .= $content;        
        $html .= "</{$tag}>";        
        return $html;
    }
    
    /**
     * link
     * 
     * Creates and returns an anchor tag.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $text [link text]
     * @param (array) $attributes [e.g. class, id, type, name]
     * @param (string) $tag [encapsulating html tag]
     * @param (array) $tag_atts [e.g. class, id for tag]
     *
     *  @return (string) [anchor tag]
     */
    public function link($text='',$attributes=[],$tag=false,$tag_atts=[])
    {
        $html  = $tag ? $this->html_open($tag,$tag_atts) : '';
        $html .= "<a{$this->_attributes($attributes)}>{$text}</a>";
        $html .= $tag ? $this->html_close($tag) : '';
        return $html;
    }

    /**
     * input
     *
     * Creates a form input (e.g. text, email, etc.).
     *
     * @since 1.0
     * @access public
     *
     * @param (array) $attributes [e.g. class, id, name]
     * @param (string) $tag [encapsulating html tag]
     * @param (array) $tag_atts [e.g. class, id for tag]
     *
     * @return (string) [html for textarea]
     */
    public function input($attributes=[],$tag=false,$tag_atts=[])
    {
        $html  = $tag ? $this->html_open($tag,$tag_atts) : '';
        $html .= "<input {$this->_attributes($attributes)}/>";
        $html .= $tag ? $this->html_close($tag) : '';
        return $html;
    }

    /**
     * textarea
     *
     * Creates a form textarea.
     *
     * @since 1.0
     * @access public
     *
     * @param (array) $attributes [e.g. class, id, name]
     * @param (string) $tag [encapsulating html tag]
     * @param (array) $tag_atts [e.g. class, id for tag]
     *
     * @return (string) [html for textarea]
     */
    public function textarea($attributes=[],$tag=false,$tag_atts=[])
    {
        $html  = $tag ? $this->html_open($tag,$tag_atts) : '';
        $html .= "<textarea{$this->_attributes($attributes,false)}";
        $html .= isset($attributes['value']) ? $attributes['value'] : '';
        $html .= '</textarea>';
        $html .= $tag ? $this->html_close($tag) : '';
        return $html;
    }

    /**
     * select
     *
     * Creates a form select box.
     *
     * @since 1.0
     * @access public
     *
     * @param (array) $select [e.g. class, id, checked, options]
     * @param (string) $tag [encapsulating html tag]
     * @param (array) $tag_atts [e.g. class, id for tag]
     *
     * @return (string) [html for select box]
     */
    public function select($select=[],$tag=false,$tag_atts=[])
    {
        $html  = $tag ? $this->html_open($tag,$tag_atts) : '';
        $html .= "<select{$this->_attributes($select['attributes'])}>";
        $html .= $this->_select_options($select['options']);
        $html .= '</select>';
        $html .= $tag ? $this->html_close($tag) : '';
        return $html;
    }

    /**
     * radio
     *
     * Creates a form radio button.
     *
     * @since 1.0
     * @access public
     *
     * @param (array) $attributes [e.g. class, id, checked]
     * @param (string) $tag [encapsulating html tag]
     * @param (array) $tag_atts [e.g. class, id for tag]
     *
     * @return (string) [html for radio button]
     */
    public function radio($attributes=[],$tag=false,$tag_atts=[])
    {
        $html  = $tag ? $this->html_open($tag,$tag_atts) : '';
        $html .= "<input type=\"radio\"{$this->_attributes($attributes)}/>";
        $attributes['attributes']['value'] = $attributes['text'];
        $html .= isset($attributes['text']) ? ' : '.self::label(['for'=>(isset($attributes['attributes']['id'])) ? $attributes['attributes']['id'] : '','value'=>$attributes['text']]) : '';
        $html .= $tag ? $this->html_close($tag) : '';
        return $html;
    }

    /**
     * checkbox
     *
     * Creates a form checkbox.
     *
     * @since 1.0
     * @access public
     *
     * @param (array) $attributes [e.g. class, id, checked]
     * @param (string) $tag [encapsulating html tag]
     * @param (array) $tag_atts [e.g. class, id for tag]
     *
     * @return (string) [html for checkbox]
     */
    public function checkbox($attributes=[],$tag=false,$tag_atts=[])
    {
        $html  = $tag ? $this->html_open($tag,$tag_atts) : '';
        $html .= "<input type=\"checkbox\"{$this->_attributes($attributes)}/>";
        $attributes['attributes']['value'] = $attributes['text'];
        $html .= isset($attributes['text']) ? ' : '.$this->label(['for'=>(isset($attributes['attributes']['id'])) ? $attributes['attributes']['id'] : '','value'=>$attributes['text']]) : '';
        $html .= $tag ? $this->html_close($tag) : '';
        return $html;
    }

    /**
     * label
     *
     * Creates a form field label.
     *
     * @since 1.0
     * @access public
     *
     * @param (array) $attributes [e.g. class, id, for]
     * @param (string) $tag [encapsulating html tag]
     * @param (array) $tag_atts [e.g. class, id for tag]
     *
     * @return (string) [html for label]
     */
    public function label($attributes=[],$tag=false,$tag_atts=[])
    {
        $html  = $tag ? $this->html_open($tag,$tag_atts) : '';
        $html .= "<label{$this->_attributes($attributes, false)}";
        $html .= isset($attributes['value']) ? $attributes['value'] : '';
        $html .= '</label>';
        $html .= $tag ? $this->html_close($tag) : '';
        return $html;
    }

    /**
     * button
     *
     * Creates a form button.
     *
     * @since 1.0
     * @access public
     *
     * @param (string) $text [button text]
     * @param (array) $attributes [e.g. class, id, type, name]
     * @param (string) $tag [encapsulating html tag]
     * @param (array) $tag_atts [e.g. class, id for tag]
     *
     * @return (string) [html for button]
     */
    public function button($text='',$attributes=[],$tag=false,$tag_atts=[])
    {
        $html  = $tag ? $this->html_open($tag,$tag_atts) : '';
        $html .= "<button{$this->_attributes($attributes)}>{$text}</button>";
        $html .= $tag ? $this->html_close($tag) : '';
        return $html;
    }

    /**
     * select_options
     *
     * Creates a string of option tags from an array
     * that comes with the select field.
     *
     * @since 1.0
     * @access private
     *
     * @param (array) $options [array of options to create]
     *
     * @return (string) [options for the select element]
     */
    private function _select_options($options=[])
    {
        $opts = '';
        foreach($options as $option) :
            $opts .= '<option';
            $opts .= isset($option['value']) ? " value=\"{$option['value']}\"" : '';
            $opts .= (isset($option['selected']) && true===$option['selected']) ? ' selected="selected"' : '';
            $opts .= '>';
            $opts .= isset($option['text']) ? $option['text'] : '';
            $opts .= '</option>';
        endforeach;
        return $opts;
    }

    /**
     * attributes
     *
     * Creates a string of attributes for labels, form fields,
     * and html elements.
     *
     * @since 1.0
     * @access private
     *
     * @param (array) $attributes [e.g. id, class, for, type, name]
     * @param (bool) $show_value [show "value attribute if passed]
     *
     * @return (string) [attributes for html]
     */
    private function _attributes($attributes=[],$show_value=true)
    {
        $atts = '';
        foreach($attributes as $attr => $value) :
            if($attr=='value' && $show_value===false):
                $atts .= '';
            else:
                $atts .= " {$attr}=\"{$value}\"";
            endif;
        endforeach;
        return $atts;
    }
}

