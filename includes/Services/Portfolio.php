<?php
/**
 * Portfolio
 * 
 * Adds metals portfolio functionality.
 * 
 * Created: 02.05.17 @ 13:34 EST
 * Modified: 02.05.17 @ 13:34 EST
 * 
 * @version 1.1
 * 
 * @package    jwplg
 * @subpackage AGSPP
 * @subpackage Servicess
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

use Agspp\Interfaces\ServiceInterface;
use Agspp\ServiceBase;

class Portfolio extends ServiceBase implements ServiceInterface
{
    /**
     * Table name for portfolio.
     * 
     * @since 1.0
     *
     * @var string $_table
     */
    private $_table;

    /**
     * Class constructor.
     * 
     * Set variables and run
     * install for this 
     * class.
     * 
     * @since 1.0
     */
    function __construct()
    {
        parent::__construct();

        /**
         * Run on 'wp' hook.
         */
        add_action('init', [$this, 'wp_action_wp'], 200);
    }

    public function set_dependencies($dependencies) {}

    /**
     * Run on 'wp' hook.
     *
     * @since 1.2
     * @access public
     */
    public function wp_action_wp()
    {
        $this->_table = $this->_db->prefix.'portfolio';
    }
    
    /**
     * Get and return user portfolio metals.
     * 
     * @since 1.0
     * 
     * @param int $user_id [user id]
     *
     * @return void null
     */
    public function get_portfolio($user_id=0)
    {
        /**
         * If no user id is passed stop.
         */
        if(!$user_id):
            return;
        endif;
        
        /**
         * Metals portfolio array to return.
         */
        $portfolio_array = [];
        
        /**
         * Gold SQL query. And add 
         * to portfolio array.
         */
        $gold_sql = "SELECT SUM(`weight`) AS 'weight' FROM `{$this->_table}` WHERE `user_id`={$user_id} AND `metal`='gold' ";
        $portfolio_array['gold'] = $this->_db->get_row($gold_sql)->weight;
        
        /**
         * Silver SQL query. And add 
         * to portfolio array.
         */
        $silver_sql     = "SELECT SUM(`weight`) AS 'weight' FROM `{$this->_table}` WHERE `user_id`={$user_id} AND `metal`='silver' ";
        $portfolio_array['silver']  = $this->_db->get_row($silver_sql)->weight;
        
        /**
         * Platinum SQL query. And add 
         * to portfolio array.
         */
        $platinum_sql   = "SELECT SUM(`weight`) AS 'weight' FROM `{$this->_table}` WHERE `user_id`={$user_id} AND `metal`='platinum' ";
        $portfolio_array['platinum']= $this->_db->get_row($platinum_sql)->weight;
        
        /**
         * Palladium SQL query. And add 
         * to portfolio array.
         */
        $palladium_sql  = "SELECT SUM(`weight`) AS 'weight' FROM `{$this->_table}` WHERE `user_id`={$user_id} AND `metal`='palladium' ";
        $portfolio_array['palladium'] = $this->_db->get_row($palladium_sql)->weight;
        
        /**
         * Return data array.
         */
        return $portfolio_array;
    }
    
    /**
     * Add metals to user's portfolio.
     * 
     * @since 1.0
     * 
     * @param object $order [order]
     * @param int $user_id [id of order's user]
     */
    public function add_to_portfolio($order,$order_id,$user_id)
    {        
        $items = $order->get_items();           
        
        foreach($items as $item):          
            /**
             * Get metal and wieght.
             */
            $metal  = (string) get_post_meta($item['product_id'], '_product_metal', 1);
            $weight = (float) get_post_meta($item['product_id'], '_product_weight', 1);
            
            /**
             * If product type is metal insert 
             * into user's portfolio.
             */
            if('metal'===get_post_meta($item['product_id'], '_product_type', 1)):                
                $this->_insert_order_metals($user_id, $order_id, $metal, $weight,$item['qty']);
            endif;            
        endforeach;
    }
    
    /**
     * Check if user order metal exists
     * in his/her portfolio.
     * 
     * @since 1.0
     * 
     * @param int $user_id [user id]
     * @param int $order_id [order id]
     * @param string $metal [metal type]
     * @param float $weight [weight of metal]
     */
    private function _check_order_metal_exists($user_id,$order_id,$metal,$weight)
    {        
        $sql =  "SELECT * FROM `{$this->_table}` WHERE `user_id`='{$user_id}' AND `order_id`='{$order_id}' AND `metal`='{$metal}' AND `weight`='{$weight}'";
        return $this->_db->get_row($sql);
    }
    
    /**
     * If order item does not exist in portfolio, 
     * then add to user's portfolio.
     * 
     * @since 1.0
     * 
     * @param int $user_id [user id]
     * @param int $order_id [order id]
     * @param string $metal [metal type]
     * @param float $weight [weight of metal]
     * @param int $qty [quantity purchased]
     */
    private function _insert_order_metals($user_id,$order_id,$metal,$weight,$qty)
    {
        /**
         * Check if order metals already in 
         * user portfolio. If not proceed.
         */
        if($this->_check_order_metal_exists($user_id, $order_id, $metal, $weight)):
            return;
        endif;
        /**
         * Run insert.
         */
        for($i=1;$i<=$qty;$i++):
            $sql = "INSERT INTO `{$this->_table}` (`user_id`,`order_id`,`metal`,`weight`) VALUES ({$user_id},{$order_id},'{$metal}',{$weight})";        
            $this->_db->query($sql);
        endfor;
    }
}