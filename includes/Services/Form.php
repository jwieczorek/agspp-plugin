<?php
namespace Agspp\Services;

/**
 * Process
 * 
 * Processes and validates a form.
 * 
 * Created: 02.03.17 @ 18:12 EST
 * Modified: 02.04.17 @ 14:25 EST
 * 
 * @version 1.0
 * 
 * @package    JWieczorek
 * @subpackage Form
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 */

class Form
{
    /**
     * Form validator.
     * 
     * @since 1.0
     * 
     * @var (object) $_validator
     */
    private $_validator;
    
    /**
     * Form fields array.
     * 
     * @since 1.0
     *
     * @var (array) $_fields
     */
    private $_fields    = [];
    
    /**
     * Cleaned form field values.
     * 
     * @since 1.0
     *
     * @var (array) $_cleaned
     */
    private $_cleaned   = [];
    
    /**
     * Validation errors.
     * 
     * @since 1.0
     *
     * @var (array) $_errors 
     */
    private $_errors    = [];

    /**
     * Class constructor.
     * 
     * Sets form validator, form fields, and cleans
     * form values.
     * 
     * @since 1.0
     * 
     * @param (array) $form_array [form elements to validate]
     */
    public function __construct()
    {
        $this->_validator   = class_exists('\Agspp\Services\Form_Field_Validator') ? new \Agspp\Services\Form_Field_Validator() : null;
    }

    /**
     * Sets form fields to clean and validate.
     *
     * @since 1.2
     * @access public
     *
     * @param array $form_array [form fields]
     */
    public function set_fields($form_array=[])
    {
        $this->_fields      = $form_array;
        $this->_clean_field_values();
    }

    /**
     * set_validator
     * 
     * Set validator.
     * 
     * Allows third party validator to be used.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (object) $validator
     */
    public function set_validator($validator=null)
    {
        $this->_validator = $validator;
    }

    /**
     * set_error
     * 
     * Set field error message.
     * 
     * This is generally called from a callback 
     * function to set a field error.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $field [name of field to set error]
     * @param (string) $message [error message]
     */
    public function set_error($field='',$message='')
    {
        $this->_errors[$field] = $message;
    }

    /**
     * has_error
     * 
     * Checks if error message exists for a specified
     * field.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $field [field for error message]
     * 
     * @return (book) [true/false if error message exists]
     */
    public function has_error($field='')
    {
        return isset($this->_errors[$field]) ? true : false;
    }

    /**
     * error
     * 
     * Returns an error message for a specified
     * field.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $field [field for error message]
     * 
     * @return (string) [error message]
     */
    public function error($field='')
    {
        return isset($this->_errors[$field]) ? $this->_errors[$field] : '';
    }
    
    /**
     * errors
     * 
     * Returns the errors array.
     * 
     * @since 1.0
     * @access public
     * 
     * @return (array) [errors array]
     */
    public function errors()
    {
        return !empty($this->_errors) ? $this->_errors : false;
    }

    /**
     * has_field
     * 
     * Checks if field value exists for a specified
     * field.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string) $field [field to check value]
     * 
     * @return (bool) [true/false if field exists]]
     */
    public function has_field($field='')
    {
        return isset($this->_cleaned[$field]) ? true : false;
    }

    /**
     * field
     * 
     * Returns a field value for specified field.
     * 
     * @param (string) $field [field to get value]
     * @return (string) [field value]
     */
    public function field($field='')
    {
        return isset($this->_cleaned[$field]) ? $this->_cleaned[$field] : '';
    }

    /**
     * process
     * 
     * Processes the form, validates the fields, and calls
     * a callback method/function.
     * 
     * @since 1.0
     * @access public
     * 
     * @param (string/array) $callback [string or object array]
     */
    public function process($callback='')
    {
        $this->_validate_fields();
        
        /**
         * If errors exists then return.
         */
        if(!empty($this->_errors)):
            return;
        endif;
        
        /**
         * If array is passed for callback try to call 
         * method. Otherwise try to callback function.
         */
        if(is_array($callback) && is_callable($callback)):
            
            call_user_func_array($callback,array($this));
        
        elseif(!is_array($callback) && function_exists($callback) && is_callable($callback)):
            
            $callback($this);
        
        endif;
    }

    /**
     * _validate_fields
     * 
     * Loop through fields and call validator on each.
     * 
     * @since 1.0
     * @access private
     */
    private function _validate_fields()
    {
        foreach($this->_fields as $field_name => $field_values) :
            
            $this->_validate_rules($field_name, $field_values);
        
        endforeach;
    }

    /**
     * _validate_rules
     * 
     * Loop through field validation rules and
     * validate.
     * 
     * @since 1.0
     * @access private
     * 
     * @param (string) $field [field name to validate]
     * @param (array) $field_array [field array with rules]
     *
     */
    private function _validate_rules($field, $field_array)
    {
        /**
         * If no rules are set then exit and move 
         * to next field.
         */
        if(!isset( $field_array['rules'] )):
            return;
        endif;

        /**
         * Loop through field rules and validate.
         */
        foreach($field_array['rules'] as $name => $rule) :
            
            /**
             * If field already has errors stop.
             */
            if(isset($this->_errors[$field])) :
                
                continue;
            endif;
            
            /**
             * If function exists by rule name call it. Otherwise 
             * call internal validator.
             */
            if(function_exists($name) && is_callable($name)) :   
                
                call_user_func_array($name, array($field,$this,$field_array));
            
            elseif($this->_validator && is_callable(array($this->_validator, $name))):
            
                call_user_func_array(array($this->_validator, $name), array($field,$this,$field_array));
            
            endif;
        
        endforeach;
    }

   /**
    * _clean_field_values
    * 
    * Loop through field array and clean field values
    * either by post or get.
    * 
    * @since 1.0
    * @access private
    */
    private function _clean_field_values()
    {
        /**
         * Loop though field and clean.
         */
        foreach($this->_fields as $field_name => $field_array) :
            
            $field_value = isset($_REQUEST[$field_name]) ? $_REQUEST[$field_name] : '';
       
            if(isset($field_array['filter'])) :
                /**
                * If custom PHP filter is set in field array
                * use it. 
                */
                $this->_cleaned[$field_name] = filter_var($field_value, $field_array['filter']);
            
            else:    
                /**
                * If  no custom PHP filter is set in field array
                * use FILTER_SANITIZE_STRING.
                */
                $this->_cleaned[$field_name] = filter_input(INPUT_POST, $field_name, FILTER_SANITIZE_STRING);
            
            endif;
        
        endforeach;
    }
}
