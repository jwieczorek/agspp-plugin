<?php


/**
 * User
 * 
 * AGSPP user class.
 * 
 * Created: 02.10.17 @ 11:53 EST
 * Modified: 02.10.17 @ 17:51 EST
 * 
 * @version 1.1
 *
 * @package    JWieczorek
 * @subpackage agspp
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services;

class User
{
    
    /**
     * WordPress user object.
     * 
     * @since 1.0
     * @access public
     *
     * @var object $user
     */
    public $user;
    
    /**
     * WordPress user id.
     * 
     * @since 1.0
     *
     * @var int $id
     */
    public $id;
    
    /**
     * Values from agspp_verify_password_reset_link()
     * 
     * @since 1.0
     *
     * @var array $_password_reset_verification
     */
    private $_password_reset_verification;
    
    /**
     * Current date/time.
     * 
     * @since 1.0
     * @access public
     *
     * @var string $date
     */
    public $date;
    
    /**
     * Class constructor run WordPress
     * actions.
     */
    public function __construct() 
    {
        /**
         * Run function on WP 'init'
         * hook.
         */
        add_action('init', [$this, 'wp_action_init'], 200);
        
        /**
         * Log user's successful login.
         */
        add_action('wp_login', [$this, 'log_login_success'], 10, 2);
        
        /**
         * Log failed login.
         */
        add_action('wp_login_failed', [$this, 'log_login_fail']);
    }  
    
    /**
     * Gets user by a specific key.
     * 
     * @since 1.0
     * 
     * @param string $key [key to get user by]
     * 
     * @return object/false)[WP_User object]
     */
    public function get_by($key='', $value='')
    {
        return get_user_by($key, _field($value));
    }
    
    /**
     * Set user from different functions.
     * 
     * @since 1.0
     */
    public function set($user)
    {
        $this->user = $user;
        $this->id   = $user ? $user->ID : 0;
    }
    
    /**
     * Inserts new user
     * 
     * @since 1.0
     * 
     * @param string $email [option email]
     * @param string $password [option password]
     * @param string $fname [option first name]
     * @param string $lname [option last name]
     */
    public function add($email=false, $password=false, $fname=false, $lname=false)
    {
        /**
         * Set first and last name
         */
        $first_name = $fname ? _field($fname) : _field('first_name');
        $last_name = $fname ? _field($lname) : _field('last_name');
        /**
         * Setup user data array
         */
        $user_data = [
            'user_login'    => $email ? _field($email) :  _field('email'),
            'user_pass'     => $password ? $password : _field('password'),  
            'user_email'    => $email ? _field($email) :  _field('email'),
            'display_name'  => $first_name.' '.$last_name,
            'first_name'    => ucfirst($first_name),
            'last_name'     => ucfirst($last_name),
            'role'          => 'customer'
        ];
        
        /**
         * Insert user, set user id and
         * update registered date and ip.
         */
        $this->id = wp_insert_user($user_data);
        $this->update_meta('_register_date', $this->date);
        $this->update_meta('_register_ip', agspp_ip());

        /**
         * Compose message and call
         * log function.
         */
        $message = sprintf("'%s' successfully registered.", ($email ? _field($email) :  _field('email')));
        _log(AGSPP_GA_LOG_PATH, $message);

        $user_log   = str_replace('{uid}', $this->id, AGSPP_USR_LOG_PATH);
        _log($user_log, $message);
    }
    
    /**
     * Deletes user from database.
     * 
     * @since 1.0
     */
    public function delete()
    {
        wp_delete_user($this->id);
    }
    
    /**
     * login
     * 
     * Logs a user into the website.
     * 
     * @since 1.0
     *
     * @param string $email [manual login email]
     * @param string $password [manual login password]
     *
     * @return true/false [whether a successful login occurred]
     */
    public function login($email=false, $password=false)
    {
        $login = wp_signon([
            'user_login'    => $email ? $email : _field('email'),
            'user_password' => $password ? $password : _field('password'),
            'remember'      => ('on'===_field('remember')) ? true : false
        ]);
        
        return is_wp_error($login) ? false : true;
    }
    
    /**
     * Get a user's meta field.
     * 
     * @since 1.0
     * 
     * @param string $key [meta key to get]
     * 
     * @return string [user's meta field]
     */
    public function meta($key='')
    {
        if(''===$key):
            return;
        endif;
        
        return get_user_meta($this->id, $key, 1);
    }
    
    /**
     * Updates a user's meta key.
     * 
     * @since 1.0
     * 
     * @param string $key [meta key to update]
     * @param string $value [value of meta to update]
     * @param int $user_id [optional user id]
     */
    public function update_meta($key='',$value='',$user_id=0)
    {
        /**
         * Set user id
         */
        $uid    = (0===$user_id) ? $this->id : $user_id;
        /**
         * If key is array then
         * update array of value.
         */
        if(is_array($key)):
            $this->_update_meta_array($key,$uid);
            return;
        endif;
        
        /**
         * If no key or value then 
         * return and stop.
         */
        if(''===$key):
            return;
        endif;
        
        /**
         * Update user meta.
         */
        update_user_meta($uid, $key, $value);
    }
    
    /**
     * Update array of meta values.
     * 
     * @since 1.0
     * 
     * @param array $meta [array of meta values to update]
     * @param int $uid [user id]
     */
    private function _update_meta_array($meta=[],$uid)
    {
        foreach($meta as $meta_key):            
            update_user_meta($uid, $meta_key, _field($meta_key));
        endforeach;        
    }
    
    /**
     * Update user's password.
     * 
     * @since 1.0
     */
    public function update_password()
    {
        wp_set_password(_field('password'), $this->id);
    }
    
    /**
     * Run on WordPress 'init' hook.
     * 
     * @since 1.0
     */
    public function wp_action_init()
    {
        /**
         * Check if is password reset
         * page.
         */
        $this->_password_reset_verification = agspp_verify_password_reset_link();
        
        /**
         * Get user.
         */
        $this->user = $this->_get_user();
        
        /**
         * Set user id.
         */
        $this->id   = $this->user ? $this->user->ID : 0;
        
        /**
         * Set date.
         */
        $this->date = date('D, F j, Y @ g:i:s a', current_time('timestamp'));
    }
    
    /**
     * Log successful login.
     * 
     * @since 1.0
     * 
     * @param string $user_login [username/email]
     * @param object $user []
     */
    public function log_login_success($user_login,$user)
    {
        /**
         * Compose message and call
         * log function.
         */
        $message = sprintf("'%s' successfully logged in.", $user_login);        
        _log(AGSPP_GA_LOG_PATH, $message);
        
        $user_log   = str_replace('{uid}', $user->ID, AGSPP_USR_LOG_PATH);
        _log($user_log, $message);
        
        /**
         * Update user's last login
         * and ip address.
         */
        update_user_meta($user->ID, '_last_login_ip', agspp_ip());
        update_user_meta($user->ID, '_last_login', $this->date);
    }
    
    /**
     * Log failed login attempts.
     * 
     * @since 1.0
     * 
     * @param string $user_login [username/email]
     */
    public function log_login_fail($user_login)
    {
        /**
         * Compose message and call
         * log function.
         */
        $message = sprintf("'%s' failed to logged in.", $user_login);        
        _log(AGSPP_GA_LOG_PATH, $message);
    }
    
    /**
     * Gets the current user.
     * 
     * @since 1.0
     * 
     * @return object [WP_User object]
     */
    private function _get_user()
    {        
        /**
         * If password reset link get user
         * by email.
         */
        if('success'===$this->_password_reset_verification['status']):
            return $this->_password_reset_verification['user'];
        elseif('expired'===$this->_password_reset_verification['status']):
            return 0;
        endif;
        
        /**
         * Otherwise get user.
         */
        return wp_get_current_user();
    }

    public function __get($property)
    {
        return property_exists($this, $property) ? $property : null;
    }
}