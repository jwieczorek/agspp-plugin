<?php
/**
 * Agspp
 * 
 * Plugin PM class.
 * 
 * Created: 02.09.17 @ 08:36 EST
 * Modified: 02.09.17 @ 08:36 EST
 * 
 * @version 1.0
 * 
 * @package    jwplg
 * @subpackage AGSPP
 * @subpackage Servicess
 *
 * @author     Joshua Wieczorek <joshuawieczorek@outlook.com>
 * @copyright (c) 2017, Joshua Wieczorek
 */

namespace Agspp\Services\PrivateMessage;

class PrivateMessage
{
    /**
     * Class constructor.
     * 
     * Set variables and run
     * install for this 
     * class.
     * 
     * @since 1.0
     */
    function __construct() 
    {
        $this->run();
    }
    
    public function run()
    {
        //add_action('init', [$this, 'run_on_init']);
    }
    
    public function run_on_init()
    {
        $labels = array(
            'name'               => 'Messages',
            'singular_name'      => 'Message',
            'menu_name'          => 'Messages',
            'name_admin_bar'     => 'Message',
            'add_new'            => 'Send Message',
            'add_new_item'       => 'Send new Message',
            'new_item'           => 'New Message',
            'edit_item'          => 'Respond to Message',
            'view_item'          => 'View Messages',
            'all_items'          => 'All Messages',
            'search_items'       => 'Search Messages',            
            'not_found'          => 'No messages found!',
            'not_found_in_trash' => 'No messages found in Trash'
	);

	$args = array(
            'labels'             => $labels,
            'description'        => __( 'Description.', 'your-plugin-textdomain' ),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'message' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => ['title', 'editor']
	);

	register_post_type( 'message', $args );

    }
    
    /**
     * install
     * 
     * Install databases and options
     * for this class.
     * 
     * @since 1.0
     * @access public
     */
    public function install()
    {
        /**
         * Get table version from options table.
         */
        $local_table_version = (float) get_option($this->_table_option);
        
        /**
         * If table is current then exit 
         * and do not install.
         */
        if($local_table_version == $this->_table_version):
            return;
        endif;
        
        /**
         * Create database table.
         */
        dbDelta($this->_db_table_sql());
        
        /**
         * Update table version option.
         */
        update_option($this->_table_option,$this->_table_version);
    }  
    
    /**
     * _db_table_sql
     * 
     * Returns sql syntax for database table.
     * 
     * @since 1.0
     * @access private
     * 
     * @return (string) [sql syntax to create table]
     */
    private function _db_table_sql()
    {
        return "CREATE TABLE `{$this->_table}` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `user_id` int(11) DEFAULT NULL,
                `order_id` int(11) DEFAULT NULL,
                `metal` varchar(20) DEFAULT NULL,
                `weight` varchar(255) DEFAULT NULL,
                PRIMARY KEY (`id`)
        ) {$this->_db->get_charset_collate()}";
    }
    
}